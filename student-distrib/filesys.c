// file system driver
#include "filesys.h"

/**************** filesys operations ****************/

/*
 *init_filesys
 *
 *DESCRIPTION:  initialize file system
 *INPUT:         filesys_start -- the starting address of the file system
 *RETURN:       void
 */
void init_filesys(const uint32_t filesys_start){
    BOOT_BLOCK  = (boot_block_t*)  filesys_start;
    DENTRY      =     (dentry_t*) ( filesys_start + 64); // 64 B is size of boot block statistics
    INODE       =      (inode_t*) ( filesys_start + FS_BLOCK_SIZE ); // skip the boot block

    // data blocks are behind boot block and all inodes
    DATA_BLOCK  = (data_block_t*) ( filesys_start + FS_BLOCK_SIZE*(1 + BOOT_BLOCK->n_inode));
}

/*
 *read_dentry_by_name
 *
 *DESCRIPTION: this function  fill in the dentry_t block passed as their second argument with the file name, file
 *             type, and inode number for the file.
 *INPUT:        fname -- the name of the file, might be > 32 char
 *              dentry -- directory entry
 *RETURN:       0 success, -1 fail which indicates the file is non exist
 */
int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry){
    uint32_t i;

    // check invalid fname or dentry ptr
    if (dentry == NULL || fname == NULL) {return -1;}

    for (i = 0; i < BOOT_BLOCK->n_dentry; ++i) {
        // compare 1st 32 char
        if( strncmp( (int8_t*)BOOT_BLOCK->dentries[i].file_name, (int8_t*)fname, MAX_FILENAME_LENGTH ) == 0 && strlen((int8_t*)fname) <= 32 ) {
            // fill in dentry block
            strncpy( (int8_t*)dentry->file_name, (int8_t*)BOOT_BLOCK->dentries[i].file_name, MAX_FILENAME_LENGTH);
            dentry->file_type   = BOOT_BLOCK->dentries[i].file_type;
            dentry->inode_index = BOOT_BLOCK->dentries[i].inode_index;
            return 0;
        }
    }

    return -1; // file does not exist
}

/*
 *read_dentry_by_index
 *
 *DESCRIPTION: this function  fill in the dentry t block passed as their second argument with the file name, file
 *             type, and inode number for the file, then return 0.
 *INPUT:        index -- index into the dentry block in boot block
 *              dentry -- directory entry to be filled in
 *RETURN:       0 success, -1 fail which indicate that index is invaild
 */
int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry){
    // invalid dentry index
    if (index >= BOOT_BLOCK->n_dentry) return -1;

    strncpy( (int8_t*)(dentry->file_name), (int8_t*)BOOT_BLOCK->dentries[index].file_name, MAX_FILENAME_LENGTH);
    dentry->file_type   = BOOT_BLOCK->dentries[index].file_type;
    dentry->inode_index = BOOT_BLOCK->dentries[index].inode_index;

    return 0;
}

/*
 *read_data
 *
 *DESCRIPTION: this function works much like the read system call, reading up to
 *             length bytes starting from position offset in the file with inode number
 *             inode and returning the number of bytes read and placed in the buffer.
 *
 *INPUT:    inode -- index node
 *          offset -- offset for the starting address when read file
 *          buf -- buffer that store the content read
 *          length -- number of bytes to be read
 *RETURN:   nubmber of bytes read when succeed, 0 when reach end of file, -1 when fail
 */
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length){

    // some variable used in the loop
    uint32_t inode_dataBlock_i;
    uint32_t curr_dataBlock_n; // block number in data block region
    uint32_t position_in_dataBlock;
    uint32_t bytes_read;

    if (inode >= BOOT_BLOCK->n_inode) return -1; // invalid inode index
    inode_t* inode_ptr = (inode_t*)(&(INODE[inode])); // must be executed after inode index check

    if (offset >= inode_ptr->length) return 0; // invalid offset
    if (buf == NULL) return -1; // invalid buf ptr

    inode_dataBlock_i = offset / FS_BLOCK_SIZE;
    position_in_dataBlock = offset % FS_BLOCK_SIZE;

    // starting dataBlock number
    curr_dataBlock_n = inode_ptr->dataBlock_index[inode_dataBlock_i];
    if( curr_dataBlock_n >= BOOT_BLOCK->n_dataBlock ) return -1;

    for(bytes_read = 0; bytes_read < length; bytes_read++, position_in_dataBlock++){
        // go to next dataBlock
        if(position_in_dataBlock >= FS_BLOCK_SIZE) {
            position_in_dataBlock = 0;
            inode_dataBlock_i++;
            // update block number
            curr_dataBlock_n = inode_ptr->dataBlock_index[inode_dataBlock_i];
            // invalid dataBlock number
            if(curr_dataBlock_n >= BOOT_BLOCK->n_dataBlock) return -1;
        }
        // reach end of file
        if (offset + bytes_read >= inode_ptr->length) break;

        memcpy(buf+bytes_read, &( DATA_BLOCK[curr_dataBlock_n].data[position_in_dataBlock] ), 1); // 1 for copy 1 byte
    }
    return bytes_read;
}

/**************** file operations ****************/

/*
 *Read File
 *
 *DESCRIPTION: reads the number of bytes data into buffer
 *INPUT: fd -- file discriptor
 *       buf -- buffer that store the file
 *       nbytes -- the number of bytes that need to read
 *RETURN: number of bytes read when successful
 */
int32_t read_file(int32_t fd_n, void* buf, int32_t nbytes){

    memset((uint8_t*)buf, NULL, nbytes);

    uint32_t inode =  terminal[ visible_terminal].pcb->fd[fd_n].inode_index;
    uint32_t offset =  terminal[ visible_terminal].pcb->fd[fd_n].file_pos;
    uint32_t nbytes_read = read_data(inode, offset, (uint8_t*)buf, nbytes);

    if (nbytes_read < 0) {return -1;} // invalid input, read_data failed

    // if succeed, update file position
     terminal[ visible_terminal].pcb->fd[fd_n].file_pos += nbytes_read;

    return nbytes_read;
}

/*
 *Write File
 *
 *DESCRIPTION: returns -1 (do nothing since it is read-only)
 *INPUT:  fd -- file discriptor
 *        buf -- buffer that store the file
 *        nbytes -- the number of bytes that need to read
 *RETURN: read-only filesys, return -1 on write
 */
int32_t write_file(int32_t fd, const void* buf, int32_t nbytes){
    return -1;
}

/*
 *Open File
 *
 *DESCRIPTION: use filename to fill in dentry
 *INPUT: filename -- file name
 *RETURN: 0 success, -1 fail
 */
int32_t open_file(const uint8_t* filename){
    return 0;
}

/*
 *Close File
 *
 *DESCRIPTION: do nothing
 *INPUT: fd -- file discriptor
 *RETURN:0 success, -1 fail
 */
int32_t close_file(int32_t fd){
    return 0;
}

/**************** dir operations ****************/

/*
 *Read Directory
 *
 *DESCRIPTION: read next file in current directory
 *INPUT: fd -- file discriptor
 *       buf -- buffer that store the file
 *       nbytes -- the number of bytes that need to read
 *RETURN:0 success, -1 fail
 */
int32_t read_dir(int32_t fd_n, void* buf, int32_t nbytes){
    dentry_t dentry;

    // index for dentry currently being read
    uint32_t index =  terminal[ visible_terminal].pcb->fd[fd_n].file_pos;

    int32_t ret = read_dentry_by_index(index, &dentry);

    if(ret == 0) {
        memcpy((uint8_t*)buf, &(dentry.file_name), MAX_FILENAME_LENGTH);
    }

    else if (ret == -1)
    {
        return 0;
    }
    index++; // next file in dir

     terminal[ visible_terminal].pcb->fd[fd_n].file_pos = index;

    if (index < MAX_NUM_DENTRY) { return nbytes;}
    else { return 0; }
}

/*
 *Write Directory
 *
 *DESCRIPTION: does nothing
 *INPUT:  fd -- file discriptor
 *        buf -- buffer that store the file
 *        nbytes -- the number of bytes that need to read
 *RETURN: read-only filesys, return -1 on write
 */
int32_t write_dir(int32_t fd, const void* buf, int32_t nbytes){
    return -1;
}

/*
 *Open Directory
 *
 *DESCRIPTION: does nothing
 *INPUT: filename -- file name
 *RETURN:0 success, -1 fail
 */
int32_t open_dir(const uint8_t* filename){
    return 0;
}

/*
 *Close Directory
 *
 *DESCRIPTION: does nothing
 *INPUT: fd -- file discriptor
 *RETURN:0 success, -1 fail
 */
int32_t close_dir(int32_t fd){
    return 0;
}

int32_t get_file_size(int32_t inode_index)
{
    return ((inode_t*)(&(INODE[inode_index])))->length;
}
