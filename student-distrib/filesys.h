// file system driver
#ifndef FILESYS_H
#define FILESYS_H

#include "lib.h"
#include "types.h"

boot_block_t*   BOOT_BLOCK;
dentry_t*       DENTRY;
inode_t*        INODE;
data_block_t*   DATA_BLOCK;
file_object_t    FILEARRAY[8];
dentry_t        global_dentry;


/********************* function declarations *********************/

/**************** basic function *****************/

void init_filesys (const uint32_t filesys_start);

extern int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry);

extern int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry);

extern int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length);

extern int32_t get_file_size(int32_t inode_index);

/**************** file operations ****************/

 int32_t read_file(int32_t fd, void* buf, int32_t nbytes);

 int32_t write_file(int32_t fd, const void* buf, int32_t nbytes);

 int32_t open_file(const uint8_t* filename);

 int32_t close_file(int32_t fd);

/**************** dir operations ****************/

 int32_t read_dir(int32_t fd, void* buf, int32_t nbytes);

 int32_t write_dir(int32_t fd, const void* buf, int32_t nbytes);

 int32_t open_dir(const uint8_t* filename);

 int32_t close_dir(int32_t fd);


#endif
