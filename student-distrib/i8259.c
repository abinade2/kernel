/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"

/* i8259_init
 * Inputs: void
 * Return Value: void
 *  Function: Initialize the 8259 PIC 
 */
void i8259_init(void) {

    //cli();//mask interrupt

    //mask all interrupts
    master_mask = 0xFF; /* IRQs 0-7  */
    slave_mask = 0xFF;  /* IRQs 8-15 */

    //initialize Master
    outb(ICW1,MASTER_8259_PORT);
    outb(ICW2_MASTER,MASTER_8259_PORT+1);
    outb(ICW3_MASTER,MASTER_8259_PORT+1);
    outb(ICW4,MASTER_8259_PORT+1);

    //initialize Slave
    outb(ICW1,SLAVE_8259_PORT);
    outb(ICW2_SLAVE,SLAVE_8259_PORT+1);
    outb(ICW3_SLAVE,SLAVE_8259_PORT+1);
    outb(ICW4,SLAVE_8259_PORT+1);

    outb(master_mask, MASTER_8259_PORT+1);
    outb(slave_mask, SLAVE_8259_PORT+1);

    enable_irq(Slave_on_Master_port);

    //sti();//unmask interrupt

    return;
}


/* enable_irq
 * Inputs: irq_num: the line that we want to enable
 * Return Value: void
 * Function: Enable (unmask) the specified IRQ
 */
void enable_irq(uint32_t irq_num) {
    
    //check boundary
    if (irq_num >= 0 && irq_num < Master_Max){
        master_mask &= ~(1 << irq_num); //aquire current mask value and calculate new mask
        outb(master_mask, MASTER_8259_PORT+1);//send signal
    }
    else if(irq_num >= Master_Max && irq_num < Slave_Max){
        slave_mask &= ~(1 << (irq_num - Master_Max));  //aquire current mask value and calculate new mask
        outb(slave_mask, SLAVE_8259_PORT+1);//send signal
    }

    return;
}



/* disable_irq
 * Inputs: irq_num: the line that we want to disable
 * Return Value: void
 * Function: disable (unmask) the specified IRQ
 */
void disable_irq(uint32_t irq_num) {

    //check boundary
    if ( (irq_num >= 0) || (irq_num <  Master_Max) ){
        master_mask = inb(MASTER_8259_PORT+1) | (1 << irq_num); //aquire current mask value and calculate new mask
        outb(master_mask,MASTER_8259_PORT+1);//send signal
    }
    else if( (irq_num >= Master_Max) || (irq_num <= Slave_Max)  ){
        slave_mask  |= (1 << (irq_num - Master_Max)); //aquire current mask value and calculate new mask
        outb(slave_mask,SLAVE_8259_PORT+1);//send signal
    }

}


/* disable_irq
 * Inputs: irq_num: the line that we want to send EOI
 * Return Value: void
 * Function: Send end-of-interrupt signal for the specified IRQ
 */
void send_eoi(uint32_t irq_num) {
    //printf("send_eoi called with: %d\n", irq_num);
    //check boundary
    if(irq_num >= 0 && irq_num < Master_Max)
    {
        outb(EOI | irq_num, MASTER_8259_PORT);
    }
    else if(irq_num >= Master_Max && irq_num < Slave_Max)
    {
        outb(EOI | (irq_num - Master_Max), SLAVE_8259_PORT);
        outb(EOI + Slave_on_Master_port, MASTER_8259_PORT);
    }

    return;
}
