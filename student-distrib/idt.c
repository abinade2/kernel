#include "idt.h"

/*
 *  FUNCTIONALITY: wrapper function to handle all interrupts
 *
 *  INPUT:  interrupt_index -- a specific interrupt vector
 *
 *  OUTPUT: none
 */
void interrupt_handler(int interrupt_index)
{
    if(interrupt_index < 0)
        exception_handler(interrupt_index);
}

/*
 *  FUNCTIONALITY: function to handle all exceptions
 *
 *  INPUT:  interrupt_index -- a specific exception vector
 *
 *  OUTPUT: none
 */
void exception_handler(int interrupt_index)
{

    switch(interrupt_index)
    {
        case -1 :
            printf("DIVIDE-BY-ZERO EXCEPTION\n");
            break;
        case -2 :
            printf("DEBUG EXCEPTION\n");
            break;
        case -3 :
            printf("NMI EXCEPTION\n");
            break;
        case -4 :
            printf("BREAKPOINT EXCEPTION\n");
            break;
        case -5 :
            printf("OVERFLOW EXCEPTION\n");
            break;
        case -6 :
            printf("BOUND RANGE EXCEEDED EXCEPTION\n");
            break;
        case -7 :
            printf("INVALID OPCODE EXCEPTION\n");
            break;
        case -8 :
            printf("DEVICE NOT AVAILABLE EXCEPTION\n");
            break;
        case -9 :
            printf("DOUBLE FAULT EXCEPTION\n");
            break;
        case -10:
            printf("SEGMENT OVERRUN EXCEPTION\n");
            break;
        case -11:
            printf("INVALID TSS EXCEPTION\n");
            break;
        case -12:
            printf("SEGMENT NOT PRESENT EXCEPTION\n");
            break;
        case -13:
            printf("STACK-SEGMENT FAULT EXCEPTION\n");
            break;
        case -14:
            printf("GENERAL PROTECTION EXCEPTION\n");
            break;
        case -15:
            printf("PAGE FAULT EXCEPTION\n");
            break;
        case -17:
            printf("MATH FAULT EXCEPTION\n");
            break;
        case -18:
            printf("ALIGNMENT CHECK EXCEPTION\n");
            break;
        case -19:
            printf("MACHINE CHECK EXCEPTION\n");
            break;
        case -20:
            printf("SIMD FP EXCEPTION EXCEPTION\n");
            break;
    }

    printf("Squashing user level program...\n");

    //return control to shell
    halt(-1);
}

/*
 *  FUNCTIONALITY: initialize IDT table
 *
 *  INPUT:  none
 *
 *  OUTPUT: none
 */
void initialize_IDT(void)
{
    int i;

    for(i = 0; i < NUM_VEC; i++)
    {
        idt[i].seg_selector  = KERNEL_CS; /* kernel code segment */
        idt[i].reserved4     = 0x0;       /* 01110 = Exception */
        idt[i].reserved2     = 0x1;
        idt[i].reserved1     = 0x1;
        idt[i].reserved0     = 0x0;
        idt[i].size          = 0x1;       /* 32-bit gate */
        idt[i].present       = 0x1;       /* interrupt is used */

        // first 32 vectors reserved for exceptions, as mandated by Intel
        if(i < 32)
        {
        	idt[i].reserved3 = 0x1;
        	idt[i].dpl       = 0x0;       /* Kernel protection level */
        }
        else
        {
        	idt[i].reserved3 = 0x0;
            idt[i].dpl       = 0x3;       /* User protection level */
        }
    }

    // put exception handler on IDT table
    SET_IDT_ENTRY(idt[0] , DIVIDE_ZERO);
    SET_IDT_ENTRY(idt[1] , DEBUG_EXCEPTION);
    SET_IDT_ENTRY(idt[2] , NMI_INTERRUPT);
    SET_IDT_ENTRY(idt[3] , BREAKPOINT_EXCEPTION);
    SET_IDT_ENTRY(idt[4] , OVERFLOW_EXCEPTION);
    SET_IDT_ENTRY(idt[5] , BOUND_RANGE);
    SET_IDT_ENTRY(idt[6] , INVALID_OPCODE);
    SET_IDT_ENTRY(idt[7] , NOT_AVAILABLE);
    SET_IDT_ENTRY(idt[8] , DOUBLE_FAULT);
    SET_IDT_ENTRY(idt[9] , SEGMENT_OVERRUN);
    SET_IDT_ENTRY(idt[10], INVALID_TSS);
    SET_IDT_ENTRY(idt[11], NOT_PRESENT);
    SET_IDT_ENTRY(idt[12], STACK_FAULT);
    SET_IDT_ENTRY(idt[13], GENERAL_PROTECTION);
    SET_IDT_ENTRY(idt[14], PAGE_FAULT);
    SET_IDT_ENTRY(idt[16], MATH_FAULT);
    SET_IDT_ENTRY(idt[17], ALIGNMENT_CHECK);
    SET_IDT_ENTRY(idt[18], MACHINE_CHECK);
    SET_IDT_ENTRY(idt[19], FLOATING_POINT);

    // insert PIT handler into IDT table
    SET_IDT_ENTRY(idt[PIT_INDEX], PIT_INTERRUPT);

    // insert KB handler into IDT table
    SET_IDT_ENTRY(idt[KEYBOARD_INDEX], KB_INTERRUPT);

    // insert RTC handler into IDT table
    SET_IDT_ENTRY(idt[RTC_INDEX], RTC_INTERRUPT);

    // insert System Call handler into IDT table
    SET_IDT_ENTRY(idt[SYS_CALL_INDEX], syscall_linkage);
}
