#ifndef _ITD_H
#define _ITD_H

#include "x86_desc.h"
#include "lib.h"
#include "syscall.h"

#define PIT_INDEX      0x20
#define KEYBOARD_INDEX 0x21
#define RTC_INDEX      0x28
#define SYS_CALL_INDEX 0x80

void interrupt_handler(int interrupt_index);

void exception_handler(int interrupt_index);

void initialize_IDT(void);

// assembly linkage declaration
void DIVIDE_ZERO(void);
void DEBUG_EXCEPTION(void);
void NMI_INTERRUPT(void);
void BREAKPOINT_EXCEPTION(void);
void OVERFLOW_EXCEPTION(void);
void BOUND_RANGE(void);
void INVALID_OPCODE(void);
void NOT_AVAILABLE(void);
void DOUBLE_FAULT(void);
void SEGMENT_OVERRUN(void);
void INVALID_TSS(void);
void NOT_PRESENT(void);
void STACK_FAULT(void);
void GENERAL_PROTECTION(void);
void PAGE_FAULT(void);
void MATH_FAULT(void);
void ALIGNMENT_CHECK(void);
void MACHINE_CHECK(void);
void FLOATING_POINT(void);
void RTC_INTERRUPT(void);
void KB_INTERRUPT(void);
void PIT_INTERRUPT(void);

#endif
