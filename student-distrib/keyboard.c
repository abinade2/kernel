#include "keyboard.h"

/*
 *  reference: https://wiki.osdev.org/PS2_Keyboard#Scan_Code_Set_1
 */

// key: alphabet
// value: scancode
typedef struct scan {
    uint8_t key;
    uint8_t value;
} scan_t;

// key: alphabet
// value: ascii value
typedef struct ascii {
    uint8_t key;
    uint8_t value;
} ascii_t;

// local functions
uint8_t scan_to_key_name(scan_t* map, uint8_t scanCode);
uint8_t key_name_to_ascii(ascii_t* map, uint8_t key_name);
void echo_key(uint8_t scanCode);



/************************ data  ************************/
// define all english key names
enum uint8_t {
    ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE,

    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

    BACKTICK, DASH, EQUAL, BRACKET_L, BRACKET_R, BACKSLASH, SEMICOLON, SINGLEQUOTE, COMMA, PERIOD, FOWARDSLASH,

    BACKSPACE, CAPS, CAPS_RELEASE, ENTER, ENTER_RELEASE, SHIFT_L, SHIFT_L_RELEASE, SHIFT_R, SHIFT_R_RELEASE,
    SPACE, TAB, TAB_RELEASE, ALT, ALT_RELEASE, CONTROL, CONTROL_RELEASE,

    F1, F2, F3, F4, F5, F6, F7, F8, F9,

    UNKNOWN_KEY
};

// array that stores scancode for each letter or symbol
static scan_t SCANCODE[SCANCODE_SIZE] = {
    {ZERO, 0x0B}, {ONE, 0x02}, {TWO, 0x03}, {THREE, 0x04}, {FOUR, 0x05}, {FIVE, 0x06}, {SIX, 0x07}, {SEVEN, 0x08}, {EIGHT, 0x09}, {NINE, 0x0A},

    {A, 0x1E}, {B, 0x30}, {C, 0x2E}, {D, 0x20}, {E, 0x12}, {F, 0x21}, {G, 0x22}, {H, 0x23}, {I, 0x17}, {J, 0x24},
    {K, 0x25}, {L, 0x26}, {M, 0x32}, {N, 0x31}, {O, 0x18}, {P, 0x19}, {Q, 0x10}, {R, 0x13}, {S, 0x1F}, {T, 0x14},
    {U, 0x16}, {V, 0x2F}, {W, 0x11}, {X, 0x2D}, {Y, 0x15}, {Z, 0x2C},

    {BACKTICK, 0x29},       {DASH, 0x0C},           {EQUAL, 0x0D},          {BRACKET_L, 0x1A},  {BRACKET_R, 0x1B},
    {BACKSLASH, 0x2B},      {SEMICOLON, 0x27},      {SINGLEQUOTE, 0x28},    {COMMA, 0x33},      {PERIOD, 0x34},
    {FOWARDSLASH, 0x35},

    {BACKSPACE, 0x0E},      {CAPS, 0x3A},           {CAPS_RELEASE, 0xBA},    {ENTER, 0x1C},
    {ENTER_RELEASE, 0x9C},   {SHIFT_L, 0x2A},         {SHIFT_L_RELEASE, 0xAA},  {SHIFT_R, 0x36},         {SHIFT_R_RELEASE, 0xB6},
    {SPACE, 0x39},          {TAB, 0x0F},            {TAB_RELEASE, 0x8F},     {ALT, 0x38},            {ALT_RELEASE, 0xB8},
    {CONTROL, 0x1D},        {CONTROL_RELEASE, 0x9D},

    {F1, 0x3B}, {F2, 0x3C}, {F3, 0x3D}, {F4, 0x3E}, {F5, 0x3F}, {F6, 0x40}, {F7, 0x41}, {F8, 0x42}, {F9, 0x43}
};

// mode 0: no  shift no  caps
// mode 1: no  shift yes caps
// mode 2: yes shift no  caps
// mode 3: yes shift yes caps

static ascii_t ASCII[MODE][ASCII_SIZE] = {

    // MODE 0

    {
        {ZERO, 0x30}, {ONE, 0x31}, {TWO, 0x32}, {THREE, 0x33}, {FOUR, 0x34}, {FIVE, 0x35}, {SIX, 0x36}, {SEVEN, 0x37}, {EIGHT, 0x38}, {NINE, 0x39},

        {A, 0x61}, {B, 0x62}, {C, 0x63}, {D, 0x64}, {E, 0x65}, {F, 0x66}, {G, 0x67}, {H, 0x68}, {I, 0x69}, {J, 0x6A},
        {K, 0x6B}, {L, 0x6C}, {M, 0x6D}, {N, 0x6E}, {O, 0x6F}, {P, 0x70}, {Q, 0x71}, {R, 0x72}, {S, 0x73}, {T, 0x74},
        {U, 0x75}, {V, 0x76}, {W, 0x77}, {X, 0x78}, {Y, 0x79}, {Z, 0x7A},

        {BACKTICK, 0x60},       {DASH, 0x2D},           {EQUAL, 0x3D},          {BRACKET_L, 0x5B},  {BRACKET_R, 0x5D},
        {BACKSLASH, 0x5C},      {SEMICOLON, 0x3B},      {SINGLEQUOTE, 0x27},    {COMMA, 0x2C},      {PERIOD, 0x2E},
        {FOWARDSLASH, 0x2F},

        {SPACE, 0x20},          {TAB, 0x09},            {BACKSPACE, 0x08},    {ENTER, 0x0D}
    },

    // MODE 1

    {
        {ZERO, 0x30}, {ONE, 0x31}, {TWO, 0x32}, {THREE, 0x33}, {FOUR, 0x34}, {FIVE, 0x35}, {SIX, 0x36}, {SEVEN, 0x37}, {EIGHT, 0x38}, {NINE, 0x39},

        {A, 0x41}, {B, 0x42}, {C, 0x43}, {D, 0x44}, {E, 0x45}, {F, 0x46}, {G, 0x47}, {H, 0x48}, {I, 0x49}, {J, 0x4A},
        {K, 0x4B}, {L, 0x4C}, {M, 0x4D}, {N, 0x4E}, {O, 0x4F}, {P, 0x50}, {Q, 0x51}, {R, 0x52}, {S, 0x53}, {T, 0x54},
        {U, 0x55}, {V, 0x56}, {W, 0x57}, {X, 0x58}, {Y, 0x59}, {Z, 0x5A},

        {BACKTICK, 0x60},       {DASH, 0x2D},           {EQUAL, 0x3D},          {BRACKET_L, 0x5B},  {BRACKET_R, 0x5D},
        {BACKSLASH, 0x5C},      {SEMICOLON, 0x3B},      {SINGLEQUOTE, 0x27},    {COMMA, 0x2C},      {PERIOD, 0x2E},
        {FOWARDSLASH, 0x2F},

        {SPACE, 0x20},          {TAB, 0x09},            {BACKSPACE, 0x08},    {ENTER, 0x0D}
    },

    // MODE 2

    {
        {ZERO, 0x29}, {ONE, 0x21}, {TWO, 0x40}, {THREE, 0x23}, {FOUR, 0x24}, {FIVE, 0x25}, {SIX, 0x5E}, {SEVEN, 0x26}, {EIGHT, 0x2A}, {NINE, 0x28},

        {A, 0x41}, {B, 0x42}, {C, 0x43}, {D, 0x44}, {E, 0x45}, {F, 0x46}, {G, 0x47}, {H, 0x48}, {I, 0x49}, {J, 0x4A},
        {K, 0x4B}, {L, 0x4C}, {M, 0x4D}, {N, 0x4E}, {O, 0x4F}, {P, 0x50}, {Q, 0x51}, {R, 0x52}, {S, 0x53}, {T, 0x54},
        {U, 0x55}, {V, 0x56}, {W, 0x57}, {X, 0x58}, {Y, 0x59}, {Z, 0x5A},

        {BACKTICK, 0x7E},       {DASH, 0x5F},           {EQUAL, 0x2B},          {BRACKET_L, 0x7B},  {BRACKET_R, 0x7D},
        {BACKSLASH, 0x7C},      {SEMICOLON, 0x3A},      {SINGLEQUOTE, 0x22},    {COMMA, 0x3C},      {PERIOD, 0x3E},
        {FOWARDSLASH, 0x3F},

        {SPACE, 0x20},          {TAB, 0x09},            {BACKSPACE, 0x08},    {ENTER, 0x0D}
    },

    // MODE 3

    {
        {ZERO, 0x29}, {ONE, 0x21}, {TWO, 0x40}, {THREE, 0x23}, {FOUR, 0x24}, {FIVE, 0x25}, {SIX, 0x5E}, {SEVEN, 0x26}, {EIGHT, 0x2A}, {NINE, 0x28},

        {A, 0x61}, {B, 0x62}, {C, 0x63}, {D, 0x64}, {E, 0x65}, {F, 0x66}, {G, 0x67}, {H, 0x68}, {I, 0x69}, {J, 0x6A},
        {K, 0x6B}, {L, 0x6C}, {M, 0x6D}, {N, 0x6E}, {O, 0x6F}, {P, 0x70}, {Q, 0x71}, {R, 0x72}, {S, 0x73}, {T, 0x74},
        {U, 0x75}, {V, 0x76}, {W, 0x77}, {X, 0x78}, {Y, 0x79}, {Z, 0x7A},

        {BACKTICK, 0x7E},       {DASH, 0x5F},           {EQUAL, 0x2B},          {BRACKET_L, 0x7B},  {BRACKET_R, 0x7D},
        {BACKSLASH, 0x7C},      {SEMICOLON, 0x3A},      {SINGLEQUOTE, 0x22},    {COMMA, 0x3C},      {PERIOD, 0x3E},
        {FOWARDSLASH, 0x3F},

        {SPACE, 0x20},          {TAB, 0x09},            {BACKSPACE, 0x08},    {ENTER, 0x0D}
    }

};

/************************ KB driver functions  ************************/
/*
 *  FUNCTIONALITY: enable interrupt for KB
 *  INPUT:  none
 *
 *  OUTPUT: none
 */
void KB_init(void) {
    // initialize modifier flags

     enter_flag[0] = enter_flag[1] = enter_flag[2] = 0;

     ctrl_flag  = 0;
     caps_flag  = 0;
     alt_flag   = 0;
     shift_flag = 0;

    enable_irq(KB_IRQ);
}

/*
 *  FUNCTIONALITY: find key name that corresponds to scanCode
 *
 *  INPUT:  map -- scan code map
 *          scanCode -- scan code of key pressed
 *
 *  OUTPUT: key_name -- english name of the key
 */
uint8_t scan_to_key_name(scan_t* map, uint8_t scanCode) {

    uint8_t key_name = UNKNOWN_KEY;

    int i = 0;

    for (i = 0; i < SCANCODE_SIZE; ++i) {
        if (map[i].value == scanCode) {
            key_name = map[i].key;
        }
    }

    return key_name;
}

/*
 *  FUNCTIONALITY: find ascii value that corresponds to key_name
 *
 *  INPUT:  map -- ascii code map
 *          key_name -- english name of the key
 *
 *  OUTPUT: ascii  -- ascii value of the key
 */
uint8_t key_name_to_ascii(ascii_t* map, uint8_t key_name) {

    uint8_t ascii;

    int i;
    for (i = 0; i < ASCII_SIZE; ++i) {
        if (map[i].key == key_name) {
            ascii = map[i].value;
        }
    }

    return ascii;
}

/*
 *  FUNCTIONALITY: print key on screen
 *
 *  INPUT: key_name -- english name of the key
 *
 *  OUTPUT: none
 *
 *  SIDE EFFECTS: writes character to console
 */
void echo_key(uint8_t key_name) {

    uint8_t ascii;

    // determine correct shift and caps lock combination

    if(!shift_flag && !caps_flag)
        ascii = key_name_to_ascii(ASCII[0], key_name);
    else if(!shift_flag && caps_flag)
        ascii = key_name_to_ascii(ASCII[1], key_name);
    else if(shift_flag && !caps_flag)
        ascii = key_name_to_ascii(ASCII[2], key_name);
    else
        ascii = key_name_to_ascii(ASCII[3], key_name);

    // NULL char, do nothing
    if (ascii == 0)
        return;

    // backspace
    if(ascii == 0x08)
    {
        if( terminal[visible_terminal].buffer_index > 0)
            putc(ascii, visible_terminal);

        buffer_backspace();

        return;
    }

    // update line buffer for terminal read and write
    if(terminal[visible_terminal].buffer_index < MAX_BUF_SIZE-2) // if less keep on writing
    {
         terminal[visible_terminal].line_buffer[terminal[visible_terminal].buffer_index] = ascii;
         terminal[visible_terminal].line_buffer[terminal[visible_terminal].buffer_index+1] = 0x10; //always finish with line feed
         terminal[visible_terminal].buffer_index++;
    }
    else if(terminal[visible_terminal].buffer_index == MAX_BUF_SIZE-2) // if one less than max write line feed
    {
        terminal[visible_terminal].line_buffer[terminal[visible_terminal].buffer_index] = '\n';
        terminal[visible_terminal].line_buffer[terminal[visible_terminal].buffer_index+1] = 0x10; //line feed ASCII character
        terminal[visible_terminal].buffer_index++;
    }
    else // otherwise if above reset buffer
    {
         terminal[visible_terminal].buffer_index = 0;
    }

    // write to screen
    if(ascii != '\n')
        putc(ascii, visible_terminal);

    return;
}

/*
 *  FUNCTIONALITY: keyboard interrupt handler
 *
 *  INPUT: none
 *
 *  OUTPUT: none
 *
 *  SIDE EFFECTS: clears or writes to console
 */
void KB_handler(void) {

    uint8_t scanCode, pressed_key;

    scanCode = inb(KB_PORT);

    //first handle special keys (caps, ctrl, shift and enter)
    switch (scanCode)
    {
        case 0x3A : //caps lock pressed

            caps_flag = (caps_flag == 1) ? 0 : 1;
            send_eoi(KB_IRQ);
            return;

        case 0xBA : //caps lock released

            send_eoi(KB_IRQ);
            return;

        case 0x1D : //ctrl pressed

            ctrl_flag = 1;
            send_eoi(KB_IRQ);
            return;

        case 0x9D : //ctrl released

            ctrl_flag = 0;
            send_eoi(KB_IRQ);
            return;

        case 0x2A : //shift pressed

            shift_flag = 1;
            send_eoi(KB_IRQ);
            return;

        case 0xAA : //shift released

            shift_flag = 0;
            send_eoi(KB_IRQ);
            return;

        case 0x38 : //alt pressed

            alt_flag = 1;
            send_eoi(KB_IRQ);
            return;

        case 0xB8 : //alt released

            alt_flag = 0;
            send_eoi(KB_IRQ);
            return;

        case 0x1C : //enter pressed

            //putc('\n', visible_terminal);
            enter_flag[visible_terminal] = 1;
            pressed_key = scan_to_key_name(SCANCODE, scanCode);
            //send_eoi(KB_IRQ);
            //return;
            break;

        case 0x9C : //enter released

            send_eoi(KB_IRQ);
            return;

        default :
            pressed_key = scan_to_key_name(SCANCODE, scanCode);
    }


    if(alt_flag && !shift_flag)
    {
        switch(pressed_key)
        {
            case F1:
                switch_terminal(0);
                break;

            case F2:
                switch_terminal(1);
                break;

            case F3:
                switch_terminal(2);
                break;

            default:
                send_eoi(KB_IRQ);
                return;
        }

    }

    restore_vid_mem();

    // 0x15 = scancode for L
    // check if ctrl-L is pressed
    if(ctrl_flag && !shift_flag && !alt_flag && pressed_key == 0x15)
    {
        clear();
        reset_cursor();

        if(terminal[visible_terminal].pcb->shell_flag)
           printf("391OS> ");

        store_vid_mem(running_terminal);
        send_eoi(KB_IRQ);
        return;
    }

    // handle unknown keys by not printing
    switch (pressed_key)
    {
        case UNKNOWN_KEY:
            break;

        default:
            echo_key(pressed_key);
    }

    store_vid_mem(running_terminal);
    send_eoi(KB_IRQ);

    return;
}
