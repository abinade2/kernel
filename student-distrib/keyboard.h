#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include "lib.h"
#include "i8259.h"
#include "syscall.h"
#include "terminal.h"
// keyboard driver for the kernel

/************************ KB driver constants ************************/

#define KB_IRQ          1
#define KB_PORT         0x60
#define MODE            4
#define SCANCODE_SIZE   72
#define ASCII_SIZE      51

// keyboard modifier flags
		 uint8_t caps_flag;
		 uint8_t shift_flag;
		 uint8_t alt_flag;
         uint8_t ctrl_flag;
volatile uint8_t enter_flag[3];

/************************ KB driver functions ************************/
extern void KB_init(void);
extern void KB_handler(void); // interrupt handler

void buffer_backspace(void);

#endif
