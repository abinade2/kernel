#include "paging.h"

/* paging_initi(alization)
 * Inputs: void
 * Return Value: void
 * Function: initializes page directories and page tables, sets registers
 */
void paging_initi(void){
    // everything that doesn't have USER is set to supervisor
    int i;

    /* Initialize the page directories and set page tables */
    for (i = 0; i < _1_KB; i++)
    {
      /* set to R/W permission for each entry */
      page_directory[i] = READ_WRITE;

      /* 4KB page tables */
      page_table[i] = (i * _4_KB) | READ_WRITE;

    }

    /* Set page table for video memory */
    page_table[VIDEO_MEM + 0] |= USER | PRESENT;
    page_table[VIDEO_MEM + 1] |= USER | PRESENT; //for 1st terminal
    page_table[VIDEO_MEM + 2] |= USER | PRESENT; //for 2nd terminal
    page_table[VIDEO_MEM + 3] |= USER | PRESENT; //for 3rd terminal

    /* Put the page table in the page directory */
    page_directory[0] = ((uint32_t)page_table) | READ_WRITE | PRESENT;

    /* Kernel 4 MB page */
    page_directory[1] = KERNEL_ADDR | SIZE | READ_WRITE | PRESENT;

    /* call assembly linkage to set registers */
    loadPageDirectory(page_directory);
    enablePaging();

    return;
}

/* restore_vid_mem
 * Inputs: none
 * Return Value: void
 * Function: restores video memory to actual memory address
 */
void restore_vid_mem(void){
    page_table[VIDEO_MEM] = (uint32_t)(VIDEO_ADDR) | USER | READ_WRITE | PRESENT;
    flush_tlb();
    return;
}

/* store_vid_mem
 * Inputs: ID of terminal
 * Return Value: void
 * Function: sets video memory according to terminal ID
 */
void store_vid_mem(int32_t term_id){
    if(visible_terminal == running_terminal)

        page_table[VIDEO_MEM] = ((uint32_t)VIDEO_ADDR) | USER | READ_WRITE | PRESENT;

    else

        page_table[VIDEO_MEM] = ((uint32_t)(VIDEO_ADDR + (term_id+1)*_4_KB )) | USER | READ_WRITE | PRESENT;

    flush_tlb();

    return;
}

/* remap_vidmem
 * Inputs: PID number of process
 * Return Value: void
 * Function: Initialises page for video memory
 */
void remap_vidmem()
{
    // 33 = 132MB virtual
    page_directory[33] = (uint32_t)(uint32_t)page_table | USER | READ_WRITE | PRESENT;

    if(visible_terminal == running_terminal)
        page_table[0] = ((uint32_t)VIDEO_ADDR) | USER | READ_WRITE | PRESENT;
        
    else
        page_table[0] = ((uint32_t)(VIDEO_ADDR + (running_terminal+1)*_4_KB )) | USER | READ_WRITE | PRESENT;

    // flush TLB
    flush_tlb();

    return;
}

/* remap_program
 * Inputs: PID number of process
 * Return Value: void
 * Function: Initialises page for user program
 */
void remap_program(uint32_t pid){

    uint32_t addr = _8_MB + pid * _4_MB;

    // 32 = 128MB virtual 
    page_directory[32] =  addr | SIZE | USER | READ_WRITE | PRESENT;

    // flush TLB
    flush_tlb();
    return;
}

/* remap_term
 * Inputs: terminal id
 * Return Value: void
 * Function: Initialises page for user program
 */
void remap_term(int32_t term_id){

    uint32_t addr =  terminal[term_id].vid_mem;

    if ( visible_terminal == term_id)
        addr = TERM_VID;

    // 33 = 132MB virtual
    page_directory[33] = ((uint32_t)vid_table) | USER | READ_WRITE | PRESENT ;
    vid_table[VIDEO_MEM] = addr | USER | READ_WRITE | PRESENT;

    // flush TLB
    flush_tlb();

    return;
}

/* remap_proc
 * Inputs: PID number of process
 * Return Value: void
 * Function: Initialises page for user program
 */
int32_t remap_proc(int32_t pid){

    if (pid < 0 || pid > MAX_PROCESS)
        return -1;

    // 32 = 128MB virtual 
    page_directory[32] = (_8_MB + _4_MB * pid) | SIZE | USER | READ_WRITE | PRESENT;

    // flush TLB
    flush_tlb();
    return 0;
}

/* flush_tlb
 * Inputs: void
 * Return Value: void
 * Function: flushes the TLB (used when changing paging)
 */
void flush_tlb(void){
    asm volatile(
        "movl %%cr3, %%eax    \n"
        "movl %%eax, %%cr3    \n"
        :
        : 
        : "memory", "cc"
    );
    return;
}
