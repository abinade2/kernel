#include "types.h"
#include "x86_desc.h"
#include "lib.h"


/*
 * reference: https://wiki.osdev.org/Paging
 *
 *'Global' flag     : if set, prevents the TLB from updating the address in its cache if CR3 is reset. Note, that the page global enable bit in CR4 must be set to enable this feature.
 *'Dirty' flag      : if is set, then the page has been written to. This flag is not updated by the CPU, and once set will not unset itself.
 *'Page Size'       : stores the page size for that specific entry. If the bit is set, then pages are 4 MiB in size. Otherwise, they are 4 KiB. Please note that 4-MiB pages require PSE to be enabled.
 *'Accessed'        : is used to discover whether a page has been read or written to. If it has, then the bit is set, otherwise, it is not. Note that, this bit will not be cleared by the CPU, so that burden falls on the OS (if it needs this bit at all).
 *'Cache Disable'   : If the bit is set, the page will not be cached. Otherwise, it will be.
 *'Write-Through'   : Write-Through abilities of the page. If the bit is set, write-through caching is enabled. If not, then write-back is enabled instead.
 *'User/Supervisor' : controls access to the page based on privilege level. If the bit is set, then the page may be accessed by all; if the bit is not set, however, only the supervisor can access it. For a page directory entry, the user bit controls access to all the pages referenced by the page directory entry. Therefore if you wish to make a page a user page, you must set the user bit in the relevant page directory entry as well as the page table entry.
 *'Read/Write'      : permissions flag. If the bit is set, the page is read/write. Otherwise when it is not set, the page is read-only. The WP bit in CR0 determines if this is only applied to userland, always giving the kernel write access (the default) or both userland and the kernel (see Intel Manuals 3A 2-20).
 *'Present'         : If the bit is set, the page is actually in physical memory at the moment. For example, when a page is swapped out, it is not in physical memory and therefore not 'Present'. If a page is called, but not present, a page fault will occur, and the OS should handle it. (See below.)
 *
 */

#define PRESENT         0x0001
#define READ_WRITE      0x0002
#define USER            0x0004
#define WRITE_THROUGHT  0x0008
#define CACHE           0x0010
#define ACCESS          0x0020
#define DIRTY           0x0040
#define SIZE            0x0080
#define GLOBAL          0x0100



/* Initializing aligned page directory and page table */
uint32_t page_table[_1_KB] __attribute__((aligned(_4_KB)));
uint32_t page_directory[_1_KB] __attribute__((aligned(_4_KB)));
uint32_t vid_table[_1_KB] __attribute__((aligned(_4_KB)));

/* paging_initi()
 * Initializing Paging.
 * Inputs: none
 * Retvals: none
 */
void paging_initi(void);


/* loadPageDirectory
 * Inputs: void
 * Return Value: void
 * Function: loading the address of page directory to an register
 */
void loadPageDirectory(unsigned int*);


/* enablePaging
 * Inputs: void
 * Return Value: void
 * Function: enable paging through x86
 */
void enablePaging(void);

extern void restore_vid_mem(void);
extern void store_vid_mem(int32_t term_id);


extern void remap_vidmem();
extern int32_t remap_proc(int32_t pid);
extern void remap_program(uint32_t pid);
extern void remap_term(int32_t term_id);

extern void flush_tlb(void);
