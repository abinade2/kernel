#include "pit.h"

// reference from https://wiki.osdev.org/PIT

/* i8253_init
 * Inputs: void
 * Return Value: void
 * Function: initializes pit
 */
void i8253_init(void){
    int32_t freq = FREQUENCY / 100; // interrupt every 10ms
    int32_t low  = freq &  0xFF;    // 0xFF = mask (send lower 8 bits)
    int32_t high = freq >> 8;       // (send higher 8 bits)

    //send command to the port
    outb(0x34, CMD_REG);           // 0x34 is port in PIC for PIT
    outb(low,  CHANNEL_0);
    outb(high, CHANNEL_0);
    enable_irq(PIT_IRQ);
    return;
}

/* pit_handler
 * Inputs: void
 * Return Value: void
 * Function: pit interrupt handler
 */
void pit_handler(void)
{

    scheduler();

    return;
}

