#include "lib.h"
#include "types.h"
#include "keyboard.h"

#define FREQUENCY 			    1193182
#define CHANNEL_0 				0x40
#define CMD_REG					0x43
#define PIT_IRQ                 0x00

void i8253_init(void);
void pit_handler(void);

void scheduler(void);
