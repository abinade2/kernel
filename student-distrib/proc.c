#include "proc.h"

/* File operations table initialized to hold function pointers */
f_table_t    dir_jumptable    = { open_dir,      close_dir,      read_dir,      write_dir       };
f_table_t    file_jumptable   = { open_file,     close_file,     read_file,     write_file      };
f_table_t    rtc_jumptable    = { rtc_open,      rtc_close,      rtc_read,      rtc_write       };
f_table_t    stdin_jumptable  = { terminal_open, terminal_close, terminal_read, terminal_write  };
f_table_t    stdout_jumptable = { terminal_open, terminal_close, terminal_read, terminal_write  };
f_table_t    no_jumptable     = { fail         , fail          , fail         , fail            };


/* The fail function
 *
 * DESCRIPTION: This function is a place holder
 *
 * RETURN: always return -1
 */
int32_t fail(){
    return -1;
}

/* The control initilization function
 *
 * DESCRIPTION: This function initialize all global variable
 *
 * RETURN: none
 */
void control_initilization(void)
{
    int i;

    /* All PID entries to present */
    for(i = 0; i < MAX_PROCESS; i++) pid_array[i] = 0;

     total_prog_count  = 0;
     visible_terminal  = 0;
     running_terminal  = 1; // initialized to 1 so we end in the first shell
     cur_pid           = 0;
}

/* The file descriptor table checking function
 *
 * DESCRIPTION: This function Checks if FD is valid
 *
 * INPUT: fd -- the slot on fd table that we want to checks
 *
 * RETURN: -1 if fd is out of range or the file is not present
 *          0 if the file is present
 */
int32_t fd_check(int32_t fd)
{
    /* First check for valid index */
    if (fd >= MIN_FILE_NUM && fd < MAX_FILE_NUM )
    {
        /* Now check if file is present */
        // // use running_terminal instead of visible_terminal !!!
        if ( terminal[running_terminal].pcb->fd[fd].flags != 0)
            return 0;
    }
    return -1;
}

/* The file descriptor table clearing function
 *
 * DESCRIPTION: This function clears a fd table slot
 *              this function is called when halt
 *
 * INPUT: fd -- the slot on fd table that we want to clears
 *
 * RETURN: none
 */
void fd_clear(int32_t fd)
{
     // use running_terminal instead of visible_terminal !!!
     terminal[running_terminal].pcb->fd[fd].fileop_ptr.read  = 0;
     terminal[running_terminal].pcb->fd[fd].fileop_ptr.write = 0;
     terminal[running_terminal].pcb->fd[fd].fileop_ptr.open  = 0;
     terminal[running_terminal].pcb->fd[fd].fileop_ptr.close = 0;
     terminal[running_terminal].pcb->fd[fd].inode_index      = 0;
     terminal[running_terminal].pcb->fd[fd].file_pos         = 0;
     terminal[running_terminal].pcb->fd[fd].flags            = 0;
     return;
}

/* The PCB initilization function
 *
 * DESCRIPTION: Initialize PCB for new process on execute
 *              This function is called when executed
 *
 * INPUT: pcb        -- pcb pointer
 *        parsed_cmd -- command
 *        argv       -- argument
 *        pid        -- process id
 *        arg_num    -- # of arguments
 *
 * RETURN: none
 */
void pcb_init(pcb_t* pcb , uint8_t* parsed_cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE], uint32_t pid, uint32_t arg_num)
{
    int i;

    /* Set FD array */
    for( i = 0; i < MAX_FILE_NUM ; i++)
    {
        pcb->fd[i].fileop_ptr.read  = 0;
        pcb->fd[i].fileop_ptr.write = 0;
        pcb->fd[i].fileop_ptr.open  = 0;
        pcb->fd[i].fileop_ptr.close = 0;
        pcb->fd[i].inode_index      = 0;
        pcb->fd[i].file_pos         = 0;
        pcb->fd[i].flags            = 0;
    }

    /* Initialze STDIN */
    pcb->fd[STDIN_FD].fileop_ptr  = (f_table_t)stdin_jumptable;
    pcb->fd[STDIN_FD].inode_index = 0;
    pcb->fd[STDIN_FD].file_pos    = 0;
    pcb->fd[STDIN_FD].flags       = 1;

    /* Initialize STDOUT */
    pcb->fd[STDOUT_FD].fileop_ptr  = (f_table_t)stdout_jumptable;
    pcb->fd[STDOUT_FD].inode_index = 0;
    pcb->fd[STDOUT_FD].file_pos    = 0;
    pcb->fd[STDOUT_FD].flags       = 1;

    /* Set the PID sent in as argument */
    pcb->pid = pid;

    pcb->scheduler_flag = 0;
    pcb->shell_flag = 0;

    /* no program other than the shell initiates another program */
    pcb->parent_pid = 0;

    pcb->arg_num = arg_num;

    memcpy(pcb->cmd, parsed_cmd, MAX_COMMAND_SIZE);

    for(i = 0; i < MAX_ARGUMENT_NUM; i++)
        memcpy(pcb->argv[i],argv[i], MAX_ARGUMENT_SIZE);

    pcb->parent_esp  = 0;
    pcb->parent_ebp  = 0;
    pcb->parent_pcb  =  terminal[running_terminal].pcb;
    pcb->tss_esp0    =  tss.esp0;

    return;
}

/* The command parsing function
 *
 * DESCRIPTION: This function parsing the command, extracting the command and the argument
 *
 * INPUT: command    -- input command
 *        parsed_cmd -- output command
 *        argv       -- output argument
 *
 * RETURN: number of arguments when successful,
 *         -2 fot exit,
 *         -1 for failure.
 */
int32_t parse_cmd(const uint8_t* command, uint8_t* parsed_cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE])
{
    int32_t i,j, command_length;
    int32_t end = 0;
    int32_t start = 0;

    /* clear parsed_cmd buffer */
    for(i = 0; i < MAX_COMMAND_SIZE; i++)
        parsed_cmd[i] = '\0';

    /* clear argument buffer */
    for(i = 0; i < MAX_ARGUMENT_NUM; i++)
    {
        for(j = 0; j < MAX_ARGUMENT_SIZE; j++)
            argv[i][j] = '\0';
    }

    /* skip spaces at start of command */
    while (command[start] == ' ')
    {
        start++;
        end++;
    }

    /* find end of command (end in space or line terminator) */
    while (command[end] != ' '   &&
           command[end] != '\0'  &&
           command[end] != AS_NL) end++;

    /* check if command is bigger than valid command size */
    if(end - start > MAX_COMMAND_SIZE)
    {
        printf("command name exceeded the maximum size of %d\n", MAX_COMMAND_SIZE);
        return -1;
    }

    /* copy command to parsed_cmd */
    for (i = 0; i < (end - start); i++)
        if(command[i+start] != 0x0D) //0x0D = \n
            parsed_cmd[i] = command[i+start];

    command_length = strlen((int8_t*)command);

    /* skip spaces (needs to be here for return value) */
    while(command[end] == ' ') end++;

    /* extracting arguments */
    for( j = 0 ; end < command_length ; j++ )
    {
        /* set new start to end of last argument */
        start = end;

        /* check if max number of arguments is reached */
        if(j >= MAX_ARGUMENT_NUM)
        {
            printf("input exceeded the maximum number of arguments %d\n", MAX_ARGUMENT_NUM);
            return -1;
        }

        /* skip spaces until argument is reached */
        while (command[start] == ' ') { start++; end++; }

        /* find end of command (end in space or line terminator) */
        while (command[end] != ' '  &&
               command[end] != '\0' &&
               command[end] != AS_NL) end++;

        /* check if argument is bigger than valid command size */
        if(end - start > MAX_ARGUMENT_SIZE)
        {
            printf("argument %d exceeded the maximum size of %d\n", j+1, MAX_ARGUMENT_SIZE);
            return -1;
        }

        /* copy argument to return array */
        for (i = 0; i < (end - start); i++)
            if(command[i+start] != 0x0D) //0x0D = \n
                argv[j][i] = command[i+start];

    }

    if (strncmp("term", (int8_t*)parsed_cmd, 4) == 0)
        printf("current terminal = %d\n", visible_terminal);

    if (strncmp("exit", (int8_t*)parsed_cmd, 4) == 0)
        return -2;

    return j;
}

/* The exit function
 *
 * DESCRIPTION: This function exit a program
 *
 * RETURN: none
 */
void exit(void)
{
    asm volatile( "call halt;");
    return;
}

/* The set_pidfunction
 *
 * DESCRIPTION: This function finds an avalible pid in pid array
 *
 * RETURN: avalibel pid hen succesed
 *         -1 when all the slot are occupied
 */
int32_t set_pid(void)
{
    int32_t i;

    /* Loop till you find a free process index */
    for (i = 0; i < MAX_PROCESS; i++)
    {
        if ( pid_array[i] == 0){
             pid_array[i] = 1;
            return i;
        }
    }
    printf("Reached maximum number of processes %d\n", MAX_PROCESS);
    return -1;
}

/* The count_pid function
 *
 * DESCRIPTION: This function counts active pid
 *
 * RETURN: # of active pid
 */
int32_t count_pid(void)
{
    int32_t i, count = -1;
    for (i = 0; i < MAX_PROCESS; i++){ if ( pid_array[i] == 1) count++; }
    return count;
}

/* The clear_pid function
 *
 * DESCRIPTION: This function clears a pid in pid array
 *
 * INPUT: pid -- the pid that we want to clear
 *
 * RETURN: # of active pid 
 */
int32_t clear_pid(int32_t pid)
{
    pid_array[pid] = 0;
    return count_pid();
}

/* The get_pcb function
 *
 * DESCRIPTION: This function calculate the pcb pointer for a given pid
 *
 * INPUT: pid -- the pid that we want to get the pcb from
 *
 * RETURN: pcb pointer that points to the pcb of the input pid
 */
pcb_t* get_pcb(uint32_t pid){
    return (pcb_t *)(_8_MB - (pid + 1) * _8_KB);
}
