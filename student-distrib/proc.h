#include "lib.h"
#include "types.h"
#include "x86_desc.h"

#include "rtc.h"
#include "keyboard.h"
#include "terminal.h"
#include "filesys.h"

extern int32_t fail(); // Error handling helper function
extern void control_initilization(void);
extern void exit(void);

/* File Descriptor helper functions */
extern int32_t fd_check(int32_t fd);
extern void    fd_clear(int32_t fd);

extern void pcb_init(pcb_t* pcb , uint8_t* cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE], uint32_t pid, uint32_t arg_num);
extern pcb_t* current_pcb(void);
extern pcb_t* get_pcb(uint32_t pid);

extern int32_t parse_cmd(const uint8_t* command, uint8_t* cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE]); /* Function for parsing commands */
extern int32_t count_pid(void);
extern int32_t set_pid(void);
extern int32_t clear_pid(int32_t pid);
