#include "rtc.h"

/* Set interrupt flag */
//interrupt_flag = 1;

/* For virtualisation */
int rtc_active[MAX_TERMINAL]       = {0,0,0};
int rtc_flag[MAX_TERMINAL]         = {1,1,1};
int rtc_counter[MAX_TERMINAL]      = {0,0,0};
int rtc_init_counter[MAX_TERMINAL] = {0,0,0};

/* rtc_init()
 * Inputs: void
 * Return Value: void
 * Function: Turn on the periodic interrupt at irq line 8,
 *           Default rate at 1024 Hz  
 */
void rtc_init(void)
{
    uint8_t prev;

    // int32_t freq;

    /* select register B, and disable NMI */
    outb(DISABLE_NMI_B, RTC_PORT);
    /* read the current value of register B */		
    prev = inb(RTC_DATA);
    /* set the index again (a read will reset the index to register D) */
    outb(DISABLE_NMI_B, RTC_PORT);
    /* write the previous value ORed with 0x40. This turns on bit 6 of register B */		
    outb(prev | BIT_SIX, RTC_DATA);	
    /* Enable interrupts */

    // freq = 2;
    // rtc_write(NULL, &freq, 4);

    enable_irq(RTC_IRQ);

}

/*
 *  FUNCTIONALITY: function to handle rtc interrupt
 *
 *  INPUT:  none
 *
 *  OUTPUT: none
 */
void rtc_interrupt(void)
{
    int i;

    /* logic to enable more than one RTC interrupt */
    outb(0x0C, 0x70); /* 0x0C = DATA to write, 0x70 = port */
    inb(0x71);        /* read from port 0x71 (RTC Data port) */

    /* Clear interrupt flag */
    for (i = 0; i < MAX_TERMINAL; i++)
    {
        rtc_counter[i]--;
        /* Check if counter has reached zero */
        if(rtc_counter[i] == 0)
        {
            /* Clear interrupt flag */
            rtc_flag[i] = 0;
            /* Reset counter */
            rtc_counter[i] = rtc_init_counter[i];
        }
    }
    // interrupt_flag = 0;

    return;
}

/* rtc_read()
 * Inputs: fd - file descriptor
 *         buf - buffer that stores the file
 *         nbytes - number of bytes to be read
 * Return Value: always returns 0
 * Function: Blocks until the next interrupt  
 */
int32_t rtc_read(int32_t fd, void* buf, int32_t nbytes)
{
    /* Set flag */
    if(rtc_active[running_terminal] == 1)
        rtc_flag[running_terminal] = 1;

    
    sti();

    /* Wait until interrupt handler clears flag */
    while (rtc_flag[running_terminal]);

    cli();

    return 0;
}

/* rtc_write()
 * Inputs: fd - file descriptor
 *         buf - buffer that stores the file
 *         nbytes - number of bytes to be write
 * Return Value: number of bytes written
 * Function: Changes RTC interrupt frequency  
 */
int32_t rtc_write(int32_t fd, const void* buf, int32_t nbytes)
{
    int32_t freq, valid_freq;

    /* Check for null pointer */
    if (buf == NULL)
      return -1;

    /* Check for size */
    if (nbytes != FOUR_BYTES)
      return -1;

    /* Get the frequency value */
    freq = *((int32_t*)buf);

    /* Check if frequency is in valid range */
    if (freq < 2 || freq > FREQ_MAX)
      return -1;

    /* Check if frequency is a power of 2 */
    valid_freq = !(freq & (freq - 1));
    if (valid_freq != 1)
      return -1;

    rtc_counter[running_terminal] = INIT_FREQ/freq;

    rtc_init_counter[running_terminal] = rtc_counter[running_terminal];

    /* Return number of bytes written */
    return FOUR_BYTES;
}

/* rtc_open()
 * Inputs: filename - name of the file         
 * Return Value: returns 0
 * Function: Sets interrupt frequency to 2 Hz  
 */
int32_t rtc_open(const uint8_t* filename)
{
    int32_t freq = 2;

    /* Set RTC to active for the running terminal */
    rtc_active[running_terminal] = 1;

    rtc_counter[running_terminal] = INIT_FREQ/freq;

    rtc_init_counter[running_terminal] = rtc_counter[running_terminal];

    return 0;
}

/* rtc_close()
 * Inputs: fd - file descriptor
 * Return Value: returns 0
 * Function: *Not virtualized yet*
 */
int32_t rtc_close(int32_t fd)
{
    rtc_active[running_terminal] = 0;
    return 0;
}

