//RTC driver for the kernel

//based on https://wiki.osdev.org/RTC

#ifndef _RTC_H
#define _RTC_H

#include "types.h"
#include "lib.h"
#include "i8259.h"

#define RTC_PORT		0x70		// Port 0x70 is used to specify an index or "register number", and to disable NMI.
#define RTC_DATA		0x71		// Port 0x71 is used to read or write from/to that byte of CMOS configuration space.
#define RTC_IRQ		    0x08

#define BIT_SIX         0x40
#define TWO_HZ          15  
#define FREQ_MAX        8192
#define INIT_FREQ       1024
#define MAX_FREQ_POW    13
#define RATE_OFFSET     16
#define FOUR_BYTES 		4

#define DISABLE_NMI_A	0x8A
#define DISABLE_NMI_B	0x8B
#define DISABLE_NMI_C	0x8C
#define REGISTER_A		0x0A
#define REGISTER_B		0x0B
#define REGISTER_C		0x0C

/* Interrupt flag for read() */
// extern int interrupt_flag;

/* Global Variables for Virtualization */
extern int rtc_active[MAX_TERMINAL];
extern int rtc_flag[MAX_TERMINAL];
extern int rtc_counter[MAX_TERMINAL];


/*
 * rtc_init()
 *
 * Initializing RTC.
 *
 * Inputs: none
 * Retvals: none
 */
void rtc_init(void);

void rtc_interrupt(void);

int32_t rtc_read(int32_t fd, void* buf, int32_t nbytes);

int32_t rtc_write(int32_t fd, const void* buf, int32_t nbytes);

int32_t rtc_open(const uint8_t* filename);

int32_t rtc_close(int32_t fd);

#endif
