#include "scheduler.h"

/* The scheduler function
 *
 * DESCRIPTION: This function switch running process every 15 ms
 * Input: NONE
 * Output: NONE
 * Side Effects: switches processes
 */
void scheduler(void){

    // get current pcb
    pcb_t * curr_pcb = terminal[running_terminal].pcb;

    // check if no program is running on that terminal
    if(curr_pcb == NULL)
    {

        // initialize pcb and set esp and ebp
        // scheduler_flag used in execute to override pcb_init ebp and esp
        pcb_t pcb;
        pcb.scheduler_flag = 1;

        //move esp and ebp to pcb
        asm volatile(
            "movl %%esp, %0       \n"
            "movl %%ebp, %1       \n"
            : "=r" (pcb.current_esp), "=r" (pcb.current_ebp)
            :
            : "memory"
        );

        // set pointer to terminal pcb
        terminal[running_terminal].pcb = &pcb;

        // perform terminal switch to the next terminal
        switch_terminal(running_terminal);

        printf("terminal = %d\n", running_terminal+1);

        // send PIT eoi now because we don't return here after execute
        send_eoi(PIT_IRQ);

        // start shell
        execute((uint8_t *)"shell");
    }

    // save esp and ebp of old process
    asm volatile(
        "movl %%esp, %0       \n"
        "movl %%ebp, %1       \n"
        : "=r" (curr_pcb->current_esp), "=r" (curr_pcb->current_ebp)
        :
        : "memory"
    );

    // Round Robin
    running_terminal = (running_terminal + 1) % MAX_TERMINAL;

    // check again for non initialized pcb
    if(terminal[running_terminal].pcb == NULL)
    {
        send_eoi(PIT_IRQ);
        return;
    }

    // remap video memory
    remap_vidmem();

    // adjust memory for terminals
    store_vid_mem(running_terminal);

    // get next process
    pcb_t* next_pcb = terminal[running_terminal].pcb;

    // update current pid, tss and remap paging
    remap_proc(next_pcb->pid);

    // store kernel stack segment and esp
    tss.ss0 = KERNEL_DS;
    tss.esp0 = _8_MB - _8_KB * (next_pcb->pid);

    // send PIT eoi now because we don't return here
    send_eoi(PIT_IRQ);

    // perform process switch by changing esp and ebp
    asm volatile(
        "movl %0, %%esp       \n"
        "movl %1, %%ebp       \n"
        :
        : "r" (next_pcb->current_esp), "r" (next_pcb->current_ebp)
        : "esp" , "ebp"
    );

    return;
}
