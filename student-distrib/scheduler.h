#include "lib.h"
#include "types.h"
#include "x86_desc.h"
#include "pit.h"

extern void scheduler(void);

/* function needed from other place */
int32_t execute (const uint8_t* command);

void switch_terminal(int32_t term_id);

int32_t remap_proc(int32_t pid);
void store_vid_mem(int32_t term_id);
void remap_vidmem();
