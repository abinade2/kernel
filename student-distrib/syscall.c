#include "syscall.h"

/* The halt system call
 *
 * int32_t halt (uint8_t status)
 *
 * DESCRIPTION: This function terminates a process, returning the specified value to its parent process.
 *
 * INPUT:       status
 *
 * RETURN: always return 0
 */
int32_t halt (uint8_t status) {

    int i;
    cli();

    pcb_t* pcb_cur =  terminal[running_terminal].pcb;
    pcb_t* pcb_prev = pcb_cur->parent_pcb;

    /* set all present flags in PCB to "Not In Use" */
    for (i = 0; i < MAX_FILE_NUM; i++)
    {
        // close all files for current pcb
        if( i > 1 && pcb_cur->fd[i].flags == 1 )
            close(i);

        pcb_cur->fd[i].fileop_ptr = no_jumptable;
    }

    if(terminal[running_terminal].terminal_prog_count > 0)
    {
        clear_pid(pcb_cur->pid);
        total_prog_count--;
        terminal[running_terminal].terminal_prog_count--;
    }

    // exiting "root shell"
    if(terminal[running_terminal].terminal_prog_count == 0)
    {
        printf("[halt] executing new shell...\n");
        execute((uint8_t *)"shell");
    }

    //------------------ Restore parent paging ---------------------------------

    remap_program( pcb_prev->pid );
    flush_tlb();

    tss.esp0 = pcb_cur->tss_esp0;

    terminal[running_terminal].pcb = pcb_prev;

    sti();

    clear_pid(pcb_cur->pid);

    //------------------ Jump to execute return --------------------------------

    asm volatile(
                 "movl %0, %%eax;"
                 "movl %1, %%esp;" // restore parent_esp
                 "movl %2, %%ebp;" // restore parent_ebp
                 "jmp  exec_ret"
                 :
                 :"r"((uint32_t)status), "r"(pcb_cur->parent_esp), "r"(pcb_cur->parent_ebp)
                 :"eax"
                 );

    return 0;
}

/* The execute system call
 *
 * int32_t execute(const uint8_t* command)
 *
 * DESCRIPTION: This function attempts to load and execute a new program,
 *              handing off the processor to the new program until it terminates.
 *
 * INPUT:       command -- a space-separated sequence of words. The first word is the file name
 *                        of the program to be executed, and the rest of the command—stripped
 *                        of leading spaces—should be provided to the new program on request
 *                        via the getargs system call.
 *
 * RETURN:      -1    -- if the command cannot be executed. (for example, program does not exist / the file not executable,)
 *              256   -- if the program dies by an exception.
 *              0-255 -- if the program executes a halt system call. (in which case the value returned is that given by the program’s call to halt)
 */
int32_t execute(const uint8_t* command)
{

    cli();

    // invalid command
    if(command == NULL) return -1;

    if(strlen((int8_t *) command) == 1)
        return 0;

    //------------------ 0 : Parse args ----------------------------------------

    uint8_t parsed_cmd[MAX_COMMAND_SIZE];
    uint8_t argv[MAX_ARGUMENT_NUM][MAX_ARGUMENT_SIZE];
    int32_t ret, arg_num; //number that indicated special command

    //if( pid == -1) return -1; // reach max process

    ret = parse_cmd(command, parsed_cmd, argv);

    // set argument number for pcb initialization
    arg_num = (ret > -1) ? ret : 0;

    if(ret == -1) return -1;

    if(ret == -2) exit();

    //------------------ 1: Executable check -----------------------------------

    if(total_prog_count >= MAX_PROCESS)
    {
        printf("[execute] Reached maximum number of processes %d\n", MAX_PROCESS);
        return 0;
    }

        dentry_t dentry;
        uint8_t  buf[4];
        uint32_t entry_point;

        //check ELF and validity of the excution file
        if ( read_dentry_by_name(parsed_cmd, &dentry) )
        {
            return -1;
        }
        if ( read_data( dentry.inode_index, 0, buf, 4) <=0 )
        {
            return -1;
        }
        // if not an executable file
        if ( (buf[0] != AS_DEL) || (buf[1] != AS_E) || (buf[2] != AS_L) || (buf[3] != AS_F) )
        {
            return -1;
        }

        int32_t pid = set_pid();

        // Get a new pid
        total_prog_count++;
        terminal[running_terminal].terminal_prog_count++;

        //get entry point from bytes 24 to 27 in each exe file
        read_data( dentry.inode_index, ENTRY_POINT, buf, 4);
        entry_point = *((uint32_t*)buf);

    //-------------------- 3: Paging -------------------------------------------

        remap_program(pid);
        flush_tlb();

    //-------------------- 4: User-level Program Loader ------------------------

        // loading executable
        read_data( dentry.inode_index, 0, (uint8_t*)PROGRAM_START, get_file_size(dentry.inode_index));

    //-------------------- 5: set up pcb----------------------------------------

        uint32_t temp_esp, temp_ebp;

        // checks if we're running shell for first time on terminal (if so, gets ebp and esp from scheduler)
        if(terminal[running_terminal].pcb != NULL && terminal[running_terminal].pcb->scheduler_flag)
        {
            temp_esp = terminal[running_terminal].pcb->current_esp;
            temp_ebp = terminal[running_terminal].pcb->current_ebp;
            terminal[running_terminal].pcb->scheduler_flag = 0;
        }else{
            temp_esp = 0;
            temp_ebp = 0;
        }

        pcb_t pcb;
        pcb_init(&pcb, parsed_cmd, argv, pid, arg_num);
        pcb_t* pcb_ptr = get_pcb(pid);

        pcb.current_esp = temp_esp;
        pcb.current_ebp = temp_ebp;

        // Save parent ESP, EBP
        asm volatile("       \n\
            movl %%ebp, %0   \n\
            movl %%esp, %1   \n\
            "
            :"=r"(pcb.parent_ebp), "=r"(pcb.parent_esp) );

        memcpy( (void*)pcb_ptr, (void*)&pcb, PCB_SIZE );

        terminal[running_terminal].pcb = pcb_ptr;

        // checks if command is shell
        if (strncmp("shell", (int8_t*)parsed_cmd, 5) == 0){
            terminal[running_terminal].pcb->shell_flag = 1;
        }

    //--------------------- 6: context switch-----------------------------------

        // update SS0 and ESP0 in Task State Segment when switching context
        tss.ss0 = KERNEL_DS;
        tss.esp0 = _8_MB - _8_KB * (terminal[running_terminal].pcb->pid);

        context_switch(entry_point);

        asm volatile( "exec_ret:" );

        return 0;
}

/* The read system call
 *
 * int32_t read(int32_t fd_n, void* buf, int32_t nbytes)
 *
 * DESCRIPTION: This function reads data from the keyboard, a file, device (RTC), or directory.
 *              This function use a jump table referenced by the task’s file array to call
 *              from a generic handler for this call into a file-type-specific function.
 *              This jump table is inserted into the file array on the open system call.
 *
 *              case 1 - keyboard   : return data from one line that has been terminated by pressing
 *                                    Enter, or as much as fits in the buffer from one such line.
 *                                    The line returned should include the line feed character.
 *
 *              case 2 - file       : data should be read to the end of the file or the end of the
 *                                    buffer provided, whichever occurs sooner.
 *
 *              case 3 - device(RTC): this call should always return 0, but only after an interrupt
 *                                    has occurred (set a flag and wait until the interrupt handler
 *                                    clears it, then return 0).
 *
 *              case 4 - directory  : only the filename should be provided (as much as fits, max 32 bytes),
 *                                    and subsequent reads should read from successive directory entries
 *                                    until the last is reached, at which point read should repeatedly
 *                                    return 0.
 *
 * INPUT:       fd     -- file discriptor
 *              buf    -- buffer that store the file
 *              nbytes -- the number of bytes that need to read
 *
 * RETURN:      1. the number of bytes read when success
 *              2. 0 when the initial file position is at or beyond the end of file,
 *                 for normal files and the directory
 *              3. -1 when there fd are out of range or indicate to STDOUT_FD
 */
int32_t read(int32_t fd_n, void* buf, int32_t nbytes)
{
    int32_t ret;

    if (fd_check(fd_n) < 0 || fd_n == STDOUT_FD) return -1; // invalid fd

    if (buf == NULL) return -1; // invalid buf

    if (nbytes < 0) return -1; // invalid nbytes

    ret = terminal[running_terminal].pcb->fd[fd_n].fileop_ptr.read(fd_n, buf, nbytes);

    return ret;
}

/*
 *The write system call
 *
 *DESCRIPTION: This function writes data to the terminal or to a device (RTC).
 *
 *          case 1 - terminal: all data should be displayed to the screen immediately.
 *
 *          case 2 - RTC     : the system call should always accept only a 4-byte integer
 *                             specifying the interrupt rate in Hz, and should set the
 *                             rate of periodic interrupts accordingly.
 *
 *INPUT: fd -- file discriptor
 *       buf -- buffer that store the file
 *       nbytes -- the number of bytes that need to read
 *
 *RETURN: 1. -1  when fail (for instance: unable to write to regular files due to the file system is read-only)
 *        2. the number of bytes written when successed.
 */
int32_t write (int32_t fd, const void* buf, int32_t nbytes){

    int32_t ret;

    if (fd_check(fd) < 0 || fd == STDIN_FD) return -1; // invalid fd

    if (buf == NULL) return -1; // invalid buf

    if (nbytes < 0) return -1; // invalid nbytes

    ret = terminal[running_terminal].pcb->fd[fd].fileop_ptr.write(fd, buf, nbytes);

    return ret;

}

/*
 *The open system call
 *
 *DESCRIPTION: This function provides access to the file system, find the directory entry corresponding
 *             to the named file, allocate an unused file descriptor, and set up any data necessary
 *             to handle the given type of file (directory, RTC device, or regular file).
 *
 *INPUT: filename -- the name of the file that we want to open
 *RETURN: -1 if the named file does not exist or no descriptors are free.
 *        fd if successed
 */
int32_t open (const uint8_t* filename){

    int i;
    int fd = -1;
    dentry_t dentry;

    // invalid file name
    if( strlen( (char*)filename ) == 0 ) return -1;

    /* File does not exist */
    if( read_dentry_by_name(filename, &(dentry)) != 0 ) return -1;

    /* File type checks */
    if( dentry.file_type < 0 || dentry.file_type > 2) return -1;

    /* Looking for available space in FD table */
    for(i = USER_FILE_START; i < MAX_FILE_NUM; i++){

        // if we can find a free fd
        if( terminal[ running_terminal].pcb->fd[i].flags == 0)
        {
            fd = i;
            break;
        }

        // no free fd
        if(i == MAX_FILE_NUM-1) return -1;
    }

     // only regular file has inode, for other file types, set inode to 0
     terminal[running_terminal].pcb->fd[fd].inode_index = (dentry.file_type == 2)? dentry.inode_index  : 0;
     terminal[running_terminal].pcb->fd[fd].file_pos    = 0;
     terminal[running_terminal].pcb->fd[fd].flags       = 1;

    // insert file specific operation table
    switch (dentry.file_type) {
    case 0:
         terminal[ running_terminal].pcb->fd[fd].fileop_ptr  = rtc_jumptable;
        break;
    case 1:
         terminal[ running_terminal].pcb->fd[fd].fileop_ptr  = dir_jumptable;
        break;
    case 2:
         terminal[ running_terminal].pcb->fd[fd].fileop_ptr  = file_jumptable;
        break;
    }

    terminal[running_terminal].pcb->fd[fd].fileop_ptr.open(filename);

    return fd;
}

/* The close system call
 *
 * int32_t close (int32_t fd)
 *
 * DESCRIPTION: This function closes the specified file descriptor and makes it available
 *              for return from later calls to open.
 *
 * NOTE:        the default descriptors (0 for input and 1 for output) can't be closed
 *
 * INPUT:       fd -- file discriptor for the file that we want to close
 *
 * RETURN:      -1 -- when Trying to close an invalid descriptor
 *               0 -- when successful closes the file.
 */
int32_t close (int32_t fd)
{

    // invalid fd
    if(fd_check(fd) < 0 || fd < USER_FILE_START)
        return -1;

    terminal[running_terminal].pcb->fd[fd].fileop_ptr.close(fd);

    fd_clear(fd);

    return 0;
}

/*
 *The getargs system call
 *
 * DESCRIPTION: This function reads the program’s command line arguments into a user-level buffer.
 *              These arguments are stored as part of the task data when a new program is loaded.
 *              Here they are merely copied into user space.
 *
 * INPUT:       buf    -- buffer that holds the file
 *              nbytes -- number of bytes that we want to COPY
 *
 * RETURN:      -1 If there are no arguments, or if the arguments and a terminal NULL (0-byte) do not fit in the buffer.
 *
 * NOTE:        The shell does not request arguments, but you should probably still
 *              initialize the shell task’s argument data to the empty string.
 */
int32_t getargs (uint8_t* buf, int32_t nbytes){

    uint8_t* argument;

    argument = (uint8_t*) (terminal[running_terminal].pcb->argv);

    //If there are no arguments
    if ( terminal[running_terminal].pcb->arg_num == 0 || nbytes <= 0 )
    {
        return -1;
    }

    // if the arguments and a terminal NULL (0-byte) do not fit in the buffer.
    nbytes = (nbytes > MAX_ARGUMENT_SIZE * MAX_ARGUMENT_NUM) ? MAX_ARGUMENT_SIZE * MAX_ARGUMENT_NUM : nbytes;

    memcpy(buf, argument, nbytes);

    return 0;
}

/* int32_t vidmap (uint8_t** screen_start)
 *
 * The vidmap system call
 *
 * DESCRIPTION: This function maps the text-mode video memory into user space at a pre-set virtual address.
 *              Although the address returned is always the same, it should be written into the memory
 *              location provided by the caller (which must be checked for validity).
 *
 * INPUT:       screen_start
 *
 * RETURN:      -1 If the location is invalid.
 *
 * NOTE:        1. To avoid adding kernel-side exception handling for this sort of check, you can simply
 *                 check whether the address falls within the address range covered by the single user-level page.
 *              2. The video memory will require you to add another page mapping for the program, in this case
 *                 a 4 kB page. It is not ok to simply change the permissions of the video page located < 4MB
 *                 and pass that address
 */
int32_t vidmap (uint8_t** screen_start)
{
    /* Check for valid argument */
    if ( screen_start == NULL)
        return -1;

    /* Get the memory location */
    int addr = (uint32_t)screen_start;

    /* Check if address falls in address range of user-level page */
    if ( addr < VID_STA || addr > VID_END)
        return -1;

    remap_vidmem();

    *screen_start = (uint8_t *)(_132_MB); // 132MB, video memory starting address

    return _132_MB;
}

/*
 * The set handler system calls
 *
 * DESCRIPTION: This function are related to signal handling and are discussed in the section Signals below.
 *
 * INPUT:       signum --
 *              handler_address --
 *
 * RETURN:
 *
 * NOTE:        Even if your operating system does not support signals, you must support these system calls;
 *              in such a case, however, you may immediately return failure from these calls.
 */
int32_t set_handler (int32_t signum, void* handler_address)
{
    return -1;
}

int32_t sigreturn (void)
{
    return -1;
}
