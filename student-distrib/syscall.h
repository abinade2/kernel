
#ifndef _SYSCALL_H
#define _SYSCALL_H

#include "lib.h"
#include "types.h"
#include "x86_desc.h"

#define PROGRAM_START   0x8048000
#define ENTRY_POINT     24
#define VID_STA         0x08000000
#define VID_END         0x08400000

#define OPEN            0
#define CLOSE           1
#define READ            2
#define WRITE           3



void syscall_linkage(); /* Assembly linkage for system cals */
void context_switch(uint32_t entry_point); /* Assembly linkage for context switch */

/* ------------------System calls------------------ */
extern int32_t halt (uint8_t status);
extern int32_t execute (const uint8_t* command);
extern int32_t read (int32_t fd, void* buf, int32_t nbytes);
extern int32_t write (int32_t fd, const void* buf, int32_t nbytes);
extern int32_t open (const uint8_t* filename);
extern int32_t close (int32_t fd);
extern int32_t getargs (uint8_t* buf, int32_t nbytes);
extern int32_t vidmap (uint8_t** screen_start);
extern int32_t set_handler (int32_t signum, void* handler_address);
extern int32_t sigreturn (void);

//-------------------function needed from other place-------------------
//from paging.h
void     flush_tlb(void);
void     remap_program(uint32_t pid);
extern void remap_vidmem();

//from proc.h
int32_t  parse_cmd(const uint8_t* command, uint8_t* cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE]); /* Function for parsing commands */
int32_t  set_pid(void);
int32_t  clear_pid(int32_t pid);
int32_t  fd_check(int32_t fd); /* File Descriptor helper functions */
void     fd_clear(int32_t fd); /* File Descriptor helper functions */
void     exit(void);
void     pcb_init(pcb_t* pcb , uint8_t* cmd, uint8_t (*argv)[MAX_ARGUMENT_SIZE], uint32_t pid, uint32_t arg_num);
pcb_t*   current_pcb(void); /* PCB helper functions */
pcb_t*   get_pcb(uint32_t pid); /* PCB helper functions */

//from filesys.h
int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry);
int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry);
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length);
int32_t get_file_size(int32_t inode_index);

#endif
