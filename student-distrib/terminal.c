#include "terminal.h"

/*
 *  FUNCTIONALITY: clears line buffer
 *
 *  INPUT: none
 *
 *  OUTPUT: none
 *
 *  SIDE EFFECTS: clear line buffer
 */
void clear_buffer(void)
{
    int i;
    // fill buffer with NULL
    for(i = 0; i < MAX_BUF_SIZE; i++)
         terminal[ visible_terminal].line_buffer[i] = NULL;

     terminal[ visible_terminal].buffer_index = 0;
}

void buffer_backspace(void)
{
    if( terminal[ visible_terminal].buffer_index > 0)
         terminal[ visible_terminal].buffer_index--;

    // update terminal buffer
     terminal[visible_terminal].line_buffer[ terminal[ visible_terminal].buffer_index] = 0x10;
}

/*
 *  FUNCTIONALITY: buffered read from terminal
 *
 *  INPUT: fd - unused, buf - pointer to buffer to send data
 *         nbytes - unused
 *
 *  OUTPUT: returns number of chars in buffer
 *
 *  SIDE EFFECTS: changes enter flag
 */
int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes)
{
    // check for invalid buffer adress
    if((int8_t*)buf == NULL)
        return -1;

    int i, size;

    //clear buffer
     terminal[running_terminal].buffer_index = 0;

    for(i = 0; i < MAX_BUF_SIZE; i++)
    {
        ((int8_t*)buf)[i] = ' ';
         terminal[running_terminal].line_buffer[i] = ' ';
    }

    sti();

    // hold inside function until enter is pressed or buffer size is reached
    while( terminal[running_terminal].buffer_index < MAX_BUF_SIZE-1 && !enter_flag[running_terminal]);

    cli();

    // copy the line buffer to the parameter buffer
    for(i = 0; i <  terminal[ running_terminal].buffer_index; i++)
        ((int8_t*)buf)[i] =  terminal[ running_terminal].line_buffer[i];

    size =  terminal[running_terminal].buffer_index;

    // reset enter flag
    enter_flag[running_terminal] = 0;

    //clear line buffer
    for(i = 0; i < MAX_BUF_SIZE; i++)
         terminal[ running_terminal].line_buffer[i] = NULL;

     terminal[running_terminal].buffer_index = 0;

    //printf("[terminal_read] size: %d\n", size);
    // return size
    return size;
}

/*
 *  FUNCTIONALITY: buffered write to terminal
 *
 *  INPUT: fd - unused, buf - pointer to buffer to send data
 *         nbytes - size to write
 *
 *  OUTPUT: returns -1 if invalid, size it wrote if valid
 *
 *  SIDE EFFECTS: writes to console
 */
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes)
{
    // checks if buf is null or overflow
    if((int8_t*)buf == NULL)
        return -1;

    //printf("[terminal write] %d\n", running_terminal);

    int32_t i;

    // prints content to terminal
    for(i = 0; i < nbytes; i++)
        putc(((int8_t*)buf)[i] , running_terminal);

    return nbytes;
}

/*
 *input: pid 
 *output: terminal id
 *function: return the terminal that the pid belong to 
 */
/*
int32_t get_term(int32_t pid){
    int32_t i;
    for(i = 0; i < 3; i++){
        if(pid == terminal[i].pcb->pid) return i;
    }
    return -1;
}*/

/*
 *  FUNCTIONALITY: opens terminal
 *
 *  INPUT: filename - unused
 *
 *  OUTPUT: returns 0
 *
 *  SIDE EFFECTS: clears console
 */
int32_t terminal_open(const uint8_t* filename)
{
    clear();
    reset_cursor();
    clear_buffer();
    return 0;
}

/*
 *  FUNCTIONALITY: closes terminal
 *
 *  INPUT: fd - unused
 *
 *  OUTPUT: returns 0
 *
 *  SIDE EFFECTS: clears line buffer
 */
int32_t terminal_close(int32_t fd)
{
    clear_buffer();
    return 0;
}

/*
 *  FUNCTIONALITY: initialize terminal
 *
 *  INPUT: None
 *
 *  OUTPUT: None
 *
 *  SIDE EFFECTS: initialize terminal
 */
void init_terminal(void)
{

    clear();
    reset_cursor();
    clear_buffer();

    int i;
	for (i = 0; i < 3; i++){
         terminal[i].pcb = (pcb_t*)NULL;
         terminal[i].terminal_x = 0;
         terminal[i].terminal_y = 0;
         terminal[i].buffer_index = 0;
         terminal[i].terminal_prog_count = 0;
	}
    // terminal[0].vid_mem = TERM_VID;
     terminal[0].vid_mem = TERM0_VID;
     terminal[1].vid_mem = TERM1_VID;
     terminal[2].vid_mem = TERM2_VID;

	return;
}

/*
 *  FUNCTIONALITY: switching terminal
 *
 *  INPUT: the terminal id of the terminal that we want to switch to
 *
 *  OUTPUT: -2 - invalid terminal ID or maximum number of processes reached
 *          -1 - trying to switch to same terminal
 *           0 - no user-level program is running in that terminal
 *           1 - user-level programs are running in that terminal
 *
 *  SIDE EFFECTS: switching terminal
 */
void switch_terminal(int32_t term_id)
{
    int32_t prev_terminal;

    //check for valid terminal id (from 0 to 2) and if we maxed out on processes
    if( term_id < 0 || term_id > 2 ||  total_prog_count > MAX_PROCESS)
        return;

    //retrieve currently visible terminal
    prev_terminal = visible_terminal;

    //check if we're not switching to the same screen
    if(term_id == prev_terminal)
        return;

    //set current terminal in control structure
    visible_terminal = term_id;

    //restore paging from 0xb8000 to prev_terminal video mapping
    restore_vid_mem();

    //store previous terminal data into memory
    memcpy( (void*) terminal[prev_terminal].vid_mem, (const void*)TERM_VID, _4_KB );

    //display new terminal video memory
    memcpy( (void*)TERM_VID, (const void*) terminal[visible_terminal].vid_mem, _4_KB );

    update_cursor(visible_terminal);

    return;
}
