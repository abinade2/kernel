#ifndef _TERMINAL_H
#define _TERMINAL_H

#include "lib.h"
#include "types.h"
#include "keyboard.h"

extern void clear_buffer(void);
extern void buffer_backspace(void);

/************************ Terminal driver system calls ***************/

extern int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes);
extern int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes);
extern int32_t terminal_open(const uint8_t* filename);
extern int32_t terminal_close(int32_t fd);
extern void    switch_terminal(int32_t term_id);

void init_terminal(void);
void copy_term_data(int32_t term_id, int32_t cmd);
void clear_term_vid(void);

//function from other place
void remap_term(int32_t term_id);
void restore_vid_mem(void);
void store_vid_mem(int32_t term_id);
int32_t execute (const uint8_t* command);

#endif
