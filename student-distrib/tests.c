#include "tests.h"

#define PASS 1
#define FAIL 0

/* format these macros as you see fit */
#define TEST_HEADER     \
    printf("[TEST %s] Running %s at %s:%d\n", __FUNCTION__, __FUNCTION__, __FILE__, __LINE__)
#define TEST_OUTPUT(name, result)   \
    printf("[TEST %s] Result = %s\n", name, (result) ? "PASS" : "FAIL");

static inline void assertion_failure(){
    /* Use exception #15 for assertions, otherwise
       reserved by Intel */
    asm volatile("int $15");
}



//--------------------------------------------------------------------------------------
/* Checkpoint 1 tests */

/* IDT Test - Example
 *
 * Asserts that first 10 IDT entries are not NULL
 * Inputs: None
 * Outputs: PASS/FAIL
 * Side Effects: None
 * Coverage: Load IDT, IDT definition
 * Files: x86_desc.h/S
 */

int idt_test(){
    TEST_HEADER;

    int i;
    int result = PASS;
    for (i = 0; i < 10; ++i){
        if ((idt[i].offset_15_00 == NULL) &&
            (idt[i].offset_31_16 == NULL)){
            assertion_failure();
            result = FAIL;
        }
    }

    return result;
}

/* Divide by zero test
 *
 * Should cause a divide by zero exception
 * Inputs: None
 * Outputs: None
 * Side Effects: crashes kernel
 * Coverage: Divide by zero Exception
 */
int divide_test(void)
{
    TEST_HEADER;

    int i = 391;
    int j = 0;
    int k;

    k = i/j;

    return 0;
}

/* RTC interrupt test
 *
 * tests rtc interrupt
 * Inputs: None
 * Outputs: PASS/FAIL
 * Side Effects: none
 * Coverage: tests if rtc successfully interrupts
 */
int rtc_test(void)
{
    int i, freq, count;

    int read_write_flag;
    int open_flag;
    int invalid_freq_flag;

    /* Buffer being printed to screen */
    //int8_t buf[3] = {'3','9','1'};

    clear();

    /* Frequency is left shifted immediately */
    freq = 1;

    read_write_flag = 1;
    open_flag = 1;
    invalid_freq_flag = 1;

    /* rtc_open() and rtc_close() test */
    if(open_flag)
    {
        /* Sets the frequency to 2 Hz */
        reset_cursor();
        printf("rtc_open() test, frequency at 2 Hz \n");
        rtc_open(0);
        //reset_cursor();
        count = 20;
        while(count > 0)
        {
            rtc_read(0, &freq, 4);

            //terminal_write(NULL, buf, 3);

            count--;
        }
        rtc_close(0);

    }

    /* rtc_read() and rtc_write() test */
    if(read_write_flag)
    {
        /* Left shift 10 times - goes upto 1024 Hz */
        for (i = 0; i < 10; i++)
        {
            reset_cursor();
            clear();
            freq = freq << 1;
            printf("rtc_write() and rtc_read() test, frequency at %d Hz \n", freq);
            /* Set the frequency */
            rtc_write(0, &freq, 4);

            /* 3 seconds! */
            count = 3 * freq;

            while (count > 0)
            {
                /*This waits for an RTC interrupt */
                rtc_read(0, &freq, 4);
                //terminal_write(NULL, buf, 3);
                count--;
            }
        }
    }

    /* rtc_write() invalid frequency test */
    if(invalid_freq_flag)
    {
        int temp;
        clear();
        reset_cursor();
        /* Test frequency */
        freq = 391;
        printf("Frequency passed to rtc_write(): %d Hz \n", freq);
        temp = rtc_write(0, &freq, 4);
        printf("rtc_write returns: %d\n", temp);
    }

    return 1;
}


/* Dereference valid address
 *
 * tests deferencing of valid address
 * Inputs: None
 * Outputs: PASS/FAIL
 * Side Effects: none
 * Coverage: tests valid address
 */
int paging_dereferancing_test1(void)
{
    TEST_HEADER;

    uint32_t* address = (uint32_t *) 0x400004; /* Valid Address */
    uint32_t a = *address;
    a++;

    return 1;
}

/* Dereference invalid address
 *
 * Should cause a page fault exception
 * Inputs: None
 * Outputs: None
 * Side Effects: crashes kernel
 * Coverage: tests values above kernel
 */
int paging_dereferancing_test2(void)
{
    TEST_HEADER;

    uint32_t* address = (uint32_t *) 0x1200000; /* Invalid Address */
    uint32_t a = *address;
    a++;

    return 0;
}

/* Dereference NULL
 *
 * Should cause a page fault exception
 * Inputs: None
 * Outputs: None
 * Side Effects: crashes kernel
 * Coverage: tests dereference NULL
 */
int paging_dereferancing_test3(void)
{
    TEST_HEADER;

    uint32_t* address = (uint32_t *) 0x0;
    uint32_t a = *address;
    a++;

    return 0;
}



//--------------------------------------------------------------------------------------
/* Checkpoint 2 tests */


/************************ file system test ****************************/
// the first function test the data structure defined in filesys.c
// the second function test the read_data function
// the third function test the read_dentry_by_index and read_dentry_by_name function
// the fourth function test the read_file and read_dir function

// not sure if we can actually call those function andd use those data structure in this file
// maybe we should move those 4 function into the filesys.c for testing


/* this function help print buffer charater by charater
 *
 * INPUT: buf -- buffer to print
 *        length -- length of buffer to print
 *
 * OUTPUT: return 0
 */
int print_buffer(uint8_t* buf, uint32_t length){
    int32_t i;
    for (i = 0; i < length; i++) {
      putc(buf[i],-1);
    }
    return 0;
}

// int filesys_test1_1(void){
//     uint32_t i = 0;
//     uint32_t max = BOOT_BLOCK->n_dentry;

//     dentry_t* file;

//     for(i = 0; i <= max; i++){
//       file = (dentry_t*)(&BOOT_BLOCK->dentries[i]);
//       printf("file #%d: %s        , type: %d", i, (char*)(&file->file_name), file->file_type );
//       if(file->file_type == 2){ printf(", size: %d\n", ( (inode_t*)( filesys_start + ( 1 + file->inode_index )*FS_BLOCK_SIZE ) )->length ); }
//       else{ printf(", size: N/A\n"); }
//     }
//     return 1;
// }

/*
 *file system test function 1: printing the list of file
 *
 *DESCRIPTION: Printing out a list of all files in the file system
 *            (their corresponding file size would be nice to see as well as a
 *            good sanity check for the next bullet point here)
 *
 *NOTE: this function tests the open_dir, read_dir, write_dir, close_dir, read_dentry_by_index function
 *
 *
int filesys_test1_2(void){
    int32_t i;
    int32_t fd = 2;
    uint8_t buf[32];
    int32_t nbytes = 17;
    uint8_t dir_name;

    dentry_t* file;

    fileObject_t directory;
    //directory.fileop_ptr = dir_jumptable;
    directory.inode_index = 0;
    directory.file_pos = 0;
    directory.flags = 0;
    FILEARRAY[fd] = directory;

    dir_name = '.';

    open_dir(&dir_name);
    printf("\nopen dir successed\n");

    for(i = 1; i <= BOOT_BLOCK->n_dentry; i++){
       file = (dentry_t*)(&BOOT_BLOCK->dentries[i-1]);
       read_dir( fd, buf, nbytes);
       printf("file #%d:", i);
       print_buffer(buf,32);
       printf(". file type: %d, file size: %d \n", file->file_type, ( (inode_t*)( filesys_start + ( 1 + file->inode_index )*FS_BLOCK_SIZE ) )->length ) ;
    }
    printf("\nread dir successed\n");

    if (write_dir(fd, buf, nbytes) < 0) {
        printf("\nwrite dir successed\n");
    }

    close_dir(fd);
    printf("\nclose dir successed\n");

    return 1;
}*/

/*
 *file system test function 2-1: printing file content base on index number
 *
 *DESCRIPTION: Printing out the contents of various different files
 *             (text, executables, small, large, etc.).
 *             For executable files, you may see garbage values
 *             This is OK, but you should most definitely see a few patterns,
 *             namely one at the beginning of the file and one at the end of the file
 *
 *NOTE: this function tests: read_dentry_by_index
 *                           read_data
 *
 * INPUT: inode -- the file of the index node that we want to print
 *        cmd -- operation condition: 0 print 16 byte, 1 print all data
 *
int filesys_test2_index(uint32_t index){

    //allocate memory for the output
    uint8_t buf[48 * 1024]; // 4MB

    dentry_t new_dentry;
    uint8_t flag = 0;
    uint8_t* fname;
    int32_t length;

    printf("\n\n ****** file system testing with index :%d ***** \n\n", index);

    read_dentry_by_index(index ,&new_dentry);
    fname = new_dentry.file_name;

    length =  ( (inode_t*)( filesys_start + ( 1 + index )*FS_BLOCK_SIZE ) )->length ;

    printf("file name is %s\n", fname);
    if( read_data(index, 0, (uint8_t*)&buf, length) != 0 ){ printf("read data failed\n"); }

    //printing the output
    printf("content of the file at index %d is:\n", index );
    print_buffer(buf,length);
    printf("read_dentry_by_index successed\n");
    printf("read data successed\n");

    while(flag == 0);//we don't want to exit, we want to wait for printing the buffer
    return 1;
}*/

/*
 *file system test function 2-2 printing file content base on file name
 *
 *DESCRIPTION: Printing out the contents of various different files
 *             (text, executables, small, large, etc.).
 *             For executable files, you may see garbage values
 *             This is OK, but you should most definitely see a few patterns,
 *             namely one at the beginning of the file and one at the end of the file
 *
 *NOTE: this function tests: read_dentry_by_name
 *                           read_data
 *
 *INPUT: cmd -- 1: very large file :  verylargetextwithverylongname.txt
 *              2: regular file    :  frame0.txt
 *              3,4,5: executable      :  ls,cat,hello
 *
int filesys_test2_name(int32_t cmd){

    //allocate memory for the output
    uint8_t  buf[48 * 1024]; // 4MB

    char* fname;
    dentry_t new_dentry;
    uint32_t index;
    int32_t length;
    uint8_t  flag = 0;

    switch(cmd){
    case 1 :
        fname = "verylargetextwithverylongname.txt";
        break;
    case 2 :
        fname = "frame0.txt";
        break;
    case 3 :
        fname = "ls";
        break;
    case 4 :
        fname = "cat";
        break;
    case 5 :
        fname = "hello";
        break;
    default:
        printf("\n\ninvalid input option\n\n");
        return 0;
    }

    printf(" ****** file system testing: %s ***** \n", fname);

    read_dentry_by_name((uint8_t*)fname, &new_dentry);
    index = new_dentry.inode_index;

    length = ( (inode_t*)( filesys_start + ( 1 + index )*FS_BLOCK_SIZE ) )->length ;

    printf("index number is %d\n", index);
    if (read_data(index, 0, buf, length) < 0){ printf("read data failed\n"); }

    //printing the output
    printf("content of the file at index %d is:\n\n", index );
    print_buffer(buf,length);
    printf("\n\nread_dentry_by_name successed");
    printf("\nread data successed\n");

    while(flag == 0);//we don't want to exit, we want to wait for printing the buffer
    return 1;
}*/

/*
 *file system test function 3
 *
 * DESCRIPTION: this function tests open_file, read_file, write_file, close_file
 *
 * INPUT: cmd name to print different file
 *
 * OUTPUT: return 0 on success
 *
int filesys_test3(int32_t cmd){

    uint8_t  buf[48 * 1024]; // 4MB

    char* fname;
    uint32_t index;
    int32_t length;
    uint8_t  flag = 0;
    int32_t fd = 3;

    fileObject_t fileObject;
    //fileObject.fileop_ptr = file_jumptable;
    fileObject.inode_index = 0;
    fileObject.file_pos = 0;
    fileObject.flags = 0;
    FILEARRAY[fd] = fileObject;

    switch(cmd){
        // large file name
        case 1 :
            fname = "verylargetextwithverylongname.tx";
            break;
        // txt file
        case 2 :
            fname = "frame1.txt";
            break;
        // non txt file
        case 3 :
            fname = "ls";
            break;
        case 4 :
            fname = "cat";
            break;
        case 5 :
            fname = "hello";
            break;
        case 6 :
            fname = "grep";
            break;
        // large file
        case 7 :
            fname = "fish";
            break;
        default:
            printf("\n\ninvalid input option\n\n");
            return 0;
    }

    printf("\n\n ****** file system testing: %s ***** \n\n", fname);

    if( open_file((uint8_t*)fname) == 0 ) {

        printf("\nopen file successed\n");
        index = global_dentry.inode_index;
        length = INODE[index].length;
        printf("content of the file at index %d with length %d is:\n", index, length );

        read_file( fd, (uint8_t*)&buf,  length);
        print_buffer(buf,length);
        printf("\nread file successed\n");

        if (write_file(fd, buf, length) < 0) {
            printf("\nwrite file successed\n");
        }

        close_file(fd);
        printf("\nclose file successed\n");
    }
    else printf("\nopen file failed\n");

    while(flag == 0);//we don't want to exit, we want to wait for printing the buffer
    return 1;
}*/

/*
 * terminal read test
 *
 * DESCRIPTION: loops to test terminal read
 *
 * INPUT: void
 *
 * OUTPUT: void
 *
 * SIDE EFFECTS: writes to terminal
 *
int terminal_read_test(void)
{
    printf("\nentered terminal_read_test\n");
    printf("type buffer limit (128) or press enter and buffer will copy itself to the next line\n");

    int8_t buf[128];
    int32_t i, size;

    //size = terminal_read(NULL, buf, 11);
    printf("\n");
    for(i = 0; i < size; i++)
    {
        printf("%c", buf[i]);
    }

    return 0;
}
*/
//--------------------------------------------------------------------------------------
/* Checkpoint 3 tests */
/* Checkpoint 4 tests */
/* Checkpoint 5 tests */

/* Test suite entry point */
void launch_tests(){

    // TEST_OUTPUT("idt_test", idt_test());
    // TEST_OUTPUT("paging test KERNEL", paging_dereferancing_test1());
    // TEST_OUTPUT("idt_test", idt_test());
    // TEST_OUTPUT("rtc_test", rtc_test());
    // TEST_OUTPUT("paging test KERNEL", paging_dereferancing_test1());

    // only run tests that crash kernel if not trying to test RTC
    // if(!TEST_INTERRUPTS)
    // {
    //     TEST_OUTPUT("paging value test", page_test());
    //     TEST_OUTPUT("paging test ABOVE KERNEL", paging_dereferancing_test2());
    //     TEST_OUTPUT("paging test NULL", paging_dereferancing_test3());
    //     TEST_OUTPUT("divide-by-zero", divide_test());
    // }

    // TEST_OUTPUT("file system test1-1: print directory (hard code) ", filesys_test1_1() );
    // uint32_t index = 38;
    // TEST_OUTPUT("file system test2-index: print file contents(read_dentry_by_index, read_data)", filesys_test2_index(index) );
    // TEST_OUTPUT("file system test2-name: print file contents(read_dentry_by_name, read_data)", filesys_test2_name(cmd) );

    /////////////////// terminal tests start here ///////////////////

    // while(1)
    // {
    //     terminal_read_test();
    // }
    /////////////////// rtc tests start here ///////////////////

    //TEST_OUTPUT("rtc_test", rtc_test());

    /////////////////// filesys tests start here ///////////////////

    // test R, W, open, close dir, read dentry by index
    //TEST_OUTPUT("file system test1-2: print directory", filesys_test1_2() );

    //uint32_t cmd = 1;
    // test R, W, open, close file, read dentry by name, read data
    //TEST_OUTPUT("file system test3: print file contents(open_file, read_file, write_file, close_file) ", filesys_test3(cmd) );
}
