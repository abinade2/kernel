#ifndef TESTS_H
#define TESTS_H

#include "rtc.h"
#include "x86_desc.h"
#include "lib.h"
#include "paging.h"
#include "filesys.h"
#include "keyboard.h"


/* set to 1 for screen to continuously print garbage */
#define TEST_INTERRUPTS 0

// test launcher
void launch_tests();

volatile int received_RTC_interrupt;

#endif /* TESTS_H */
