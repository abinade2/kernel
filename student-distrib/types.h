/* types.h - Defines to use the familiar explicitly-sized types in this
 * OS (uint32_t, int8_t, etc.).  This is necessary because we don't want
 * to include <stdint.h> when building this OS
 * vim:ts=4 noexpandtab
 */

#ifndef _TYPES_H
#define _TYPES_H

//for file system
#define     FS_BLOCK_SIZE               4096
#define     MAX_DATA_BLOCK_INDEX        1023
#define     MAX_FILENAME_LENGTH         32
#define     MAX_NUM_DENTRY              63
#define     DENTRY_RESERVED             24
#define     BOOT_RESERVED               52

//for PCB
#define     PCB_SIZE                    400
#define     MAX_FILE_NUM                8
#define     MIN_FILE_NUM                0
#define     MAX_COMMAND_SIZE            10
#define     MAX_ARGUMENT_NUM            1
#define     MAX_ARGUMENT_SIZE           40

//for terminal
#define     MAX_BUF_SIZE                128
#define     MAX_TERMINAL                3
#define     COL_NUM                     80
#define     ROW_NUM                     25
#define     TERM_VID                    0xB8000  //this is the address of the display video memory of the termianl
#define     TERM0_VID                   0xB9000
#define     TERM1_VID                   0xBA000
#define     TERM2_VID                   0xBB000

//for process
//We're using placeholders like paging setting the page directory or table
//NON_EXIST = b00
//RUNNING =   b01
//WAITING =   b10
#define     NON_EXIST                   0x00
#define     RUNNING                     0x01
#define     WAITING                     0x02
#define     MAX_PROCESS                 6

//special place in memory
#define     KERNEL_ADDR                 0x00400000
#define     VIDEO_ADDR                  0x000B8000  /* Shifting required to write to registers correctl */
#define     VIDEO_MEM                   0xB8        /* Shifting required to write to registers correctly */
#define     STACK                       0x8400000   /* 132 MB */

#define     TABLE_SIZE                  4
#define     USER_FILE_START             2
#define     STDIN_FD                    0
#define     STDOUT_FD                   1


//special character
#define     NULL                        0

#define     _1_MB                       0x100000
#define     _2_MB                       0x200000
#define     _4_MB                       0x400000
#define     _8_MB                       0x800000
#define     _128_MB                     0x8000000
#define     _132_MB                     0x8400000

#define     _1_KB                       1024
#define     _4_KB                       4096
#define     _8_KB                       0x2000

#define     AS_DEL                      0x7F
#define     AS_E                        0x45
#define     AS_L                        0x4C
#define     AS_F                        0x46
#define     AS_NL                       0x0A




#ifndef ASM

/* Types defined here just like in <stdint.h> */
typedef int int32_t;
typedef unsigned int uint32_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef char int8_t;
typedef unsigned char uint8_t;

/*---------------------------- File System Structure -------------------------*/

/* Inode structure for file system - Appendix A */
typedef struct inode {
    uint32_t    length;
    uint32_t    dataBlock_index[MAX_DATA_BLOCK_INDEX];
} inode_t;

/* Data block structure */
typedef struct dataBlock{
    uint8_t     data[FS_BLOCK_SIZE];
} data_block_t;

/* Dentry structure */
typedef struct dentry {
    uint8_t     file_name[MAX_FILENAME_LENGTH];
    uint32_t    file_type;
    uint32_t    inode_index;
    uint8_t     reserved[DENTRY_RESERVED];
} dentry_t;

/* Boot block structure */
typedef struct boot {
    uint32_t    n_dentry;
    uint32_t    n_inode;
    uint32_t    n_dataBlock;
    uint8_t     reserved[BOOT_RESERVED];
    dentry_t    dentries[MAX_NUM_DENTRY];
} boot_block_t;

/*---------------------------- PCB Structure ----------------------------*/

/* File operations table pointer */
typedef struct function_table {
    int32_t (*open)(const uint8_t* filename);
    int32_t (*close)(int32_t fd);
    int32_t (*read)(int32_t fd, void* buf, int32_t nbytes);
    int32_t (*write)(int32_t fd, const void* buf, int32_t nbytes);
} f_table_t;

/* File Object structure - for PCB */
typedef struct file_object {
    f_table_t   fileop_ptr;     /* Only valid for data file */
    uint32_t    inode_index;
    uint32_t    file_pos;       /* Current position in file, updated by system calls */
    uint32_t    flags;          /* If flag is set, file object is in use */
} file_object_t;

/* Process control block structure */
typedef struct pcb {

    file_object_t   fd[MAX_FILE_NUM];                          /* File descriptor array */

    int32_t         shell_flag;                                /* indicates if pcb is shell */
    int32_t         scheduler_flag;                            /* overrides esp and ebp setting in pcb_init */

    uint32_t        pid;                                       /* Holds current process ID */
    uint32_t        current_esp;                               /* Store Current esp for scheduling */
    uint32_t        current_ebp;                               /* Store Current ebp for scheduling */

    struct pcb*     parent_pcb;
    uint32_t        parent_pid;                                /* Holds parent process ID */
    uint32_t        parent_esp;                                /* Stores Kernel ESP, EBP*/
    uint32_t        parent_ebp;                                /* Stores Kernel ESP, EBP*/
                   
    uint32_t        tss_esp0;                                  /* saves TSS before context switch */
                   
    uint32_t        arg_num;                                   /* Number of arguments in argv */
    uint8_t         cmd[MAX_COMMAND_SIZE];                     /* Command */
    uint8_t         argv[MAX_ARGUMENT_NUM][MAX_ARGUMENT_SIZE]; /* Argument table */

} pcb_t;

/*---------------------------- Terminal Structure ----------------------------*/

/* Terminal structure - Up to 3 terminals supported */
typedef struct terminal {
         pcb_t*     pcb;
         int32_t    terminal_x; // cursor location
         int32_t    terminal_y; // cursor location

         int32_t    vid_mem;
         int32_t    terminal_prog_count;

volatile uint8_t    line_buffer[MAX_BUF_SIZE];
volatile int32_t    buffer_index;

} terminal_t;


/*---------------------------- Global variable -------------------------------*/

    uint32_t    filesys_start;              /* Starting place for file system */

    terminal_t  terminal[MAX_TERMINAL];     /* Terminal array that store the information for each termianl */
    int32_t     visible_terminal;           /* */
    int32_t     running_terminal;           /* */

    int32_t     pid_array[MAX_PROCESS];     /* PID array that store information for each pid */
    int32_t     total_prog_count;           /* Counter for the total running program */
    int32_t     cur_pid;                    /* PID of Current running program */
    pcb_t *     curr_pcb;                   /* PCB pointer of Current running program */

    /* Function table for system call read/write/open/close */
    f_table_t dir_jumptable    ;
    f_table_t file_jumptable   ;
    f_table_t rtc_jumptable    ;
    f_table_t stdin_jumptable  ;
    f_table_t stdout_jumptable ;
    f_table_t no_jumptable     ;

#endif /* ASM */

#endif /* _TYPES_H */
